<?php

namespace Tests\Dropcart\Api;

use Dropcart\Api\Exceptions\Token\{EmptyKeyException,
    EnvKeysIdenticalException,
    KeyMissingException,
    KeysIdenticalException};
use Dropcart\Api\Token;
use Faker;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Dropcart\Api\Token
 */
class TokenTest extends TestCase
{
    private static ?Faker\Generator $faker = null;

    private function makeFaker(): Faker\Generator
    {
        return static::$faker
            ?? static::$faker = Faker\Factory::create();
    }

    /**
     * @covers ::get
     * @dataProvider dataProviderGetException
     * @throws EmptyKeyException
     * @throws KeysIdenticalException
     */
    public function testGetException(string $expectException, string $publicKey, string $privateKey): void
    {
        $this->expectException($expectException);

        (new Token())
            ->setPublicKey($publicKey)
            ->setPrivateKey($privateKey)
            ->get();
    }

    /**
     * @covers ::get
     * @throws EmptyKeyException
     * @throws KeysIdenticalException
     */
    public function testGet(): void
    {
        $faker = $this->makeFaker();

        $publicKey = $faker->unique()->sentence;
        $privateKey = $faker->unique()->sentence;

        $token = (new Token())
            ->setPublicKey($publicKey)
            ->setPrivateKey($privateKey)
            ->get();

        $this->assertTrue(Token::verify($token, $privateKey));
    }

    /**
     * @covers ::setKeyPairFromEnvironment
     * @dataProvider dataProviderSetKeyPairFromEnvironmentException
     * @throws EnvKeysIdenticalException
     * @throws KeyMissingException
     */
    public function testSetKeyPairFromEnvironmentException(
        string $expectException,
        string $envPublicKey,
        string $envPrivateKey,
        ?string $publicKey,
        ?string $privateKey
    ): void {
        $this->expectException($expectException);

        if ($publicKey !== null) {
            putenv($envPublicKey . '=' . $publicKey);
        }

        if ($privateKey !== null) {
            putenv($envPrivateKey . '=' . $privateKey);
        }

        (new Token())->setKeyPairFromEnvironment($envPublicKey, $envPrivateKey);
    }

    /**
     * @covers ::setKeyPairFromEnvironment
     * @throws EnvKeysIdenticalException
     * @throws KeyMissingException
     * @throws EmptyKeyException
     * @throws KeysIdenticalException
     */
    public function testSetKeyPairFromEnvironment(): void
    {
        $faker = $this->makeFaker();

        $publicKey = $faker->unique()->sha256;
        $privateKey = $faker->unique()->sha256;

        putenv(Token::ENV_PUBLIC_KEY . '=' . $publicKey);
        putenv(Token::ENV_PRIVATE_KEY . '=' . $privateKey);

        $token = (new Token())->setKeyPairFromEnvironment()->get();

        $this->assertTrue(Token::verify($token, $privateKey));
    }

    public function dataProviderSetKeyPairFromEnvironmentException(): array
    {
        $faker = $this->makeFaker();

        return [
            'use the same environment variable for both public and private key' => [
                EnvKeysIdenticalException::class,
                Token::ENV_PUBLIC_KEY,
                Token::ENV_PUBLIC_KEY,
                null,
                null,
            ],
            'use default environment variables, unset' => [
                KeyMissingException::class,
                Token::ENV_PUBLIC_KEY,
                Token::ENV_PRIVATE_KEY,
                null,
                null,
            ],
            'use default environment variables, empty' => [
                KeyMissingException::class,
                Token::ENV_PUBLIC_KEY,
                Token::ENV_PRIVATE_KEY,
                '',
                '',
            ],
            'use default environment variables, public key empty' => [
                KeyMissingException::class,
                Token::ENV_PUBLIC_KEY,
                Token::ENV_PRIVATE_KEY,
                '',
                'bar',
            ],
            'use default environment variables, private key empty' => [
                KeyMissingException::class,
                Token::ENV_PUBLIC_KEY,
                Token::ENV_PRIVATE_KEY,
                'foo',
                '',
            ],
            'use random environment variables, unset' => [
                KeyMissingException::class,
                $faker->unique()->word,
                $faker->unique()->word,
                null,
                null,
            ],
            'use random environment variables, empty' => [
                KeyMissingException::class,
                $faker->unique()->word,
                $faker->unique()->word,
                '',
                '',
            ],
            'use random environment variables, public key empty' => [
                KeyMissingException::class,
                $faker->unique()->word,
                $faker->unique()->word,
                '',
                'bar',
            ],
            'use random environment variables, private key empty' => [
                KeyMissingException::class,
                $faker->unique()->word,
                $faker->unique()->word,
                'foo',
                '',
            ],
        ];
    }

    public function dataProviderGetException(): array
    {
        $faker = $this->makeFaker();

        return [
            'empty public key' => [
                EmptyKeyException::class,
                '',
                $faker->sentence,
            ],
            'empty private key' => [
                EmptyKeyException::class,
                $faker->sentence,
                '',
            ],
            'both keys empty' => [
                EmptyKeyException::class,
                '',
                '',
            ],
            'identical keys' => [
                KeysIdenticalException::class,
                $sentence = $faker->sentence,
                $sentence,
            ],
        ];
    }
}
