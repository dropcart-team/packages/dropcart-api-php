<?php

namespace Tests\Dropcart\Api;

use Dropcart\Api\Config;
use Dropcart\Api\Exceptions\Config\{InsecureUriException, InvalidApiVersionException, MalformedUriException};
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Dropcart\Api\Config
 */
class ConfigTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::getBaseUri
     */
    public function testDefault(): void
    {
        $baseUri = (new Config())->getBaseUri();
        $this->assertEquals(Config::API_URI . '/' . Config::API_VERSION . '/', $baseUri);
    }

    /**
     * @covers ::setApiSecure
     * @covers ::setApiUri
     * @throws InsecureUriException
     * @throws MalformedUriException
     */
    public function testSetApiSecureException(): void
    {
        $this->expectException(InsecureUriException::class);

        (new Config())
            ->setApiSecure(true)
            ->setApiUri('http://foo.bar');
    }

    /**
     * @covers ::setApiSecure
     * @covers ::setapiUri
     * @covers ::getBaseUri
     * @throws InsecureUriException
     * @throws MalformedUriException
     */
    public function testSetApiSecure(): void
    {
        $insecureUri = 'http://foo.bar';

        $baseUri = (new Config())
            ->setApiSecure(false)
            ->setApiUri($insecureUri)
            ->getBaseUri();

        $this->assertEquals($insecureUri . '/' . Config::API_VERSION . '/', $baseUri);
    }

    /**
     * @covers ::setApiUri
     * @dataProvider dataProviderSetApiUriMalformedException
     * @throws InsecureUriException
     * @throws MalformedUriException
     */
    public function testSetApiUriMalformedException(string $uri): void
    {
        $this->expectException(MalformedUriException::class);

        (new Config())->setApiUri($uri);
    }

    /**
     * @covers ::setApiUri
     * @covers ::getBaseUri
     * @dataProvider dataProviderSetApiUrl
     * @throws InsecureUriException
     * @throws MalformedUriException
     */
    public function testSetApiUri(string $uri): void
    {
        $baseUri = (new Config())
            ->setApiUri($uri)
            ->getBaseUri();

        $this->assertEquals($uri . '/' . Config::API_VERSION . '/', $baseUri);
    }

    /**
     * @covers ::setApiVersion
     * @dataProvider dataProviderSetApiVersionException
     * @throws InvalidApiVersionException
     */
    public function testSetApiVersionException(string $version): void
    {
        $this->expectException(InvalidApiVersionException::class);

        (new Config())->setApiVersion($version);
    }

    /**
     * @covers ::setApiVersion
     * @covers ::getBaseUri
     * @dataProvider dataProviderSetApiVersion
     * @throws InvalidApiVersionException
     */
    public function testSetApiVersion(string $version): void
    {
        $baseUri = (new Config())
            ->setApiVersion($version)
            ->getBaseUri();

        $this->assertEquals(Config::API_URI . '/' . $version . '/', $baseUri);
    }

    public function dataProviderSetApiUriMalformedException(): array
    {
        return [
            'empty' => [''],
            'no protocol' => ['foo'],
            'empty protocol' => ['://foo'],
            'bogus protocol' => ['bar://foo'],
            'with slash in middle' => ['https://foo/bar'],
            'invalid dash (1)' => ['https://foo.-bar'],
            'invalid dash (2)' => ['https://foo-.bar'],
        ];
    }

    public function dataProviderSetApiUrl(): array
    {
        return [
            'TLD only' => ['https://foo'],
            'TLD only with dash' => ['https://foo-bar'],
            'FQDN' => ['https://foo.bar'],
            'FQDN with dash' => ['https://foo-bar.baz'],
            'FQDN with dashes and dots' => ['https://foo-bar.baz-boo'],
        ];
    }

    public function dataProviderSetApiVersionException(): array
    {
        return [
            'does not start with v' => ['3'],
            'only v' => ['v'],
            'does not start with v and number' => ['v.'],
            'trailing dot' => ['v3.'],
            'invalid character (dash)' => ['v3-'],
            'invalid character (letter)' => ['v3a'],
        ];
    }

    public function dataProviderSetApiVersion(): array
    {
        return [
            'v-number' => ['v3'],
            'v-number-dot-number' => ['v3.10'],
        ];
    }
}
