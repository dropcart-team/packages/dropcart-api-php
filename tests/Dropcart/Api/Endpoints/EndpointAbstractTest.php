<?php

namespace Tests\Dropcart\Api\Endpoints;

use Dropcart\Api\Client;
use Dropcart\Api\Endpoints\EndpointAbstract;
use Dropcart\Api\Request;
use Mockery;
use PHPUnit\Framework\TestCase;

class EndpointAbstractTest extends TestCase
{
    protected Client $client;
    protected Request $request;
    protected EndpointAbstract $endpointAbstract;

    protected function setUp(): void
    {
        $this->client = Mockery::mock(Client::class);
        $this->request = Mockery::mock(Request::class);

        $this->endpointAbstract = new class($this->client, '/foo', $this->request) extends EndpointAbstract {
            protected $resourcePath = 'bar/';

            public function getClient(): Client
            {
                return parent::getClient();
            }

            public function getResourcePath(): string
            {
                return $this->resourcePath;
            }

            public function getRequest(): Request
            {
                return $this->request;
            }
        };
    }

    public function testGetClient(): void
    {
        $this->assertEquals($this->client, $this->endpointAbstract->getClient());
    }

    public function testResourcePath(): void
    {
        $this->assertEquals('foo/bar', $this->endpointAbstract->getResourcePath());
    }

    public function testRequest(): void
    {
        $this->assertEquals($this->request, $this->endpointAbstract->getRequest());
    }
}
