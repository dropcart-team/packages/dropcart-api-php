<?php

namespace Tests\Dropcart\Api;

use Dropcart\Api\Client;
use Dropcart\Api\Endpoints\StoreEndpoint;
use Dropcart\Api\Endpoints\SupplierEndpoint;
use Dropcart\Api\Exceptions\DropcartApiException;
use Dropcart\Api\Exceptions\Response\DuplicateModelException;
use Dropcart\Api\Exceptions\Response\ForbiddenException;
use Dropcart\Api\Exceptions\Response\InternalServerErrorException;
use Dropcart\Api\Exceptions\Response\ModelNotFoundException;
use Dropcart\Api\Exceptions\Response\PreconditionFailureException;
use Dropcart\Api\Exceptions\Response\UnauthorizedException;
use Dropcart\Api\Exceptions\Response\UnknownResponseException;
use Dropcart\Api\Request;
use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Token;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Dropcart\Api\Client
 */
class ClientTest extends TestCase
{
    protected static ResourceInterface $testResource;
    protected static Client $client;

    public function dataProviderRequest(): array
    {
        static::$testResource = new class implements ResourceInterface {
            public ?object $data;

            public function init(?object $data, bool $processOnlyId = false): ResourceInterface
            {
                $this->data = $data;
                return $this;
            }
        };

        $mockHandler = new MockHandler(
            [
                new Response(200),
                new Response(200, [], '{"response":"Mock"}'),
                new Response(201, [], '{"response":"Another mock"}'),
                new Response(204),
                new Response(401),
                new Response(403),
                new Response(404),
                new Response(409),
                new Response(422, [], '["Mock error"]'),
                new Response(500),
                new Response(501),
            ]
        );

        static::$client = new Client(
            (new Token())
                ->setPrivateKey('2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae') // foo
                ->setPublicKey('fcde2b2edba56bf408601fb721fe9b5c338d10ee429ea04fae5511b68fbf8fb9'), // bar
            null,
            new GuzzleHttpClient(['handler' => HandlerStack::create($mockHandler)])
        );

        return [
            'PUT/PATCH' => [null],
            'GET' => [static::$testResource],
            'POST' => [static::$testResource],
            'DELETE' => [null],
            'Unauthorized' => [null, UnauthorizedException::class],
            'Forbidden' => [null, ForbiddenException::class],
            'Not Found' => [null, ModelNotFoundException::class],
            'Duplicate Model' => [null, DuplicateModelException::class],
            'Precondition Failure' => [null, PreconditionFailureException::class],
            'Internal Server Error' => [null, InternalServerErrorException::class],
            'Unknown' => [null, UnknownResponseException::class],
        ];
    }

    /**
     * @covers ::request
     * @dataProvider dataProviderRequest
     * @param ResourceInterface|null $resource
     * @param string|null $expectException
     * @throws DropcartApiException
     * @throws GuzzleException
     *
     */
    public function testRequest(?ResourceInterface $resource, ?string $expectException = null)
    {
        if ($expectException !== null) {
            $this->expectException($expectException);
        }

        $receivedResource = static::$client->request(new Request(), $resource);
        if ($resource === null) {
            $this->assertNull($receivedResource);
        } else {
            $this->assertNotEmpty($receivedResource->data);
        }
    }

    /**
     * @covers ::store
     */
    public function testStoreEndpoint()
    {
        $this->assertInstanceOf(StoreEndpoint::class, static::$client->store());
    }

    /**
     * @covers ::supplier
     */
    public function testSupplierEndpoint()
    {
        $this->assertInstanceOf(SupplierEndpoint::class, static::$client->supplier());
    }
}
