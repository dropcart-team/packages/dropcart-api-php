<?php

namespace Tests\Dropcart\Api\Resources;

use Dropcart\Api\Exceptions\Resources\InvalidDataException;
use Dropcart\Api\Resources\CollectionAbstract;
use Dropcart\Api\Resources\ResourceAbstract;
use Dropcart\Api\Resources\ResourceInterface;
use Faker\Factory as FakerFactory;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * @coversDefaultClass \Dropcart\Api\Resources\CollectionAbstract
 */
class CollectionAbstractTest extends TestCase
{
    protected CollectionAbstract $collection;

    public function dataProviderIterator(): array
    {
        return [
            'zero' => [0],
            'one' => [1],
            'random number, but more than one' => [FakerFactory::create()->numberBetween(2, 25)],
        ];
    }

    protected function setUp(): void
    {
        $this->collection = new class() extends CollectionAbstract {
            protected function getResourceClass(): ResourceInterface
            {
                return new class extends ResourceAbstract {
                    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
                    {
                        return $this;
                    }
                };
            }
        };
    }

    /**
     * @throws InvalidDataException
     */
    public function testInit(): void
    {
        $faker = FakerFactory::create();

        $obj = (object)[
            'total' =>  $faker->randomNumber(),
            'current_page' =>  $faker->randomNumber(),
            'per_page' =>  $faker->randomNumber(),
            'last_page' =>  $faker->randomNumber(),
        ];

        $this->collection->init($obj);

        $this->assertEquals($obj->total, $this->collection->getTotalCount());
        $this->assertEquals($obj->current_page, $this->collection->getCurrentPage());
        $this->assertEquals($obj->per_page, $this->collection->getPerPage());
        $this->assertEquals($obj->last_page, $this->collection->getLastPage());

        $obj->data = [new stdClass()];
        $this->collection->init($obj);

        $data = $this->collection->getData();
        $this->assertEquals(1, count($data));
        $this->assertInstanceOf(ResourceInterface::class, $this->collection->getData()[0]);
    }

    /**
     * @throws InvalidDataException
     */
    public function testInitDataException(): void
    {
        $this->expectException(InvalidDataException::class);
        /** @noinspection PhpParamsInspection */
        $this->collection->initData(['foo']);
    }

    /**
     * @throws InvalidDataException
     */
    public function testInitData(): void
    {
        $this->collection->initData([new stdClass()]);
        $this->assertEquals(1, count($this->collection->getData()));
        $this->assertInstanceOf(ResourceInterface::class, $this->collection->getData()[0]);
    }

    /**
     * @dataProvider dataProviderIterator
     * @throws InvalidDataException
     */
    public function testIteratorCountable(int $size): void
    {
        $this->collection->initData(array_pad([], $size, new stdClass()));

        $index = 0;

        foreach ($this->collection as $key => $value) {
            $this->assertEquals($index, $key);
            $this->assertIsObject($value);

            $index++;
        }

        $this->assertEquals($size, $index);
        $this->assertEquals($size, count($this->collection));
    }
}
