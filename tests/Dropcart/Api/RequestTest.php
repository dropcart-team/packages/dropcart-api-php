<?php

namespace Tests\Dropcart\Api;

use Dropcart\Api\Exceptions\Request\InvalidMethodException;
use Dropcart\Api\Exceptions\Request\InvalidResourceException;
use Dropcart\Api\Request;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Dropcart\Api\Request
 */
class RequestTest extends TestCase
{
    protected ?Request $request = null;

    protected function makeRequest(): Request
    {
        return $this->request
            ?? $this->request = new Request();
    }

    /**
     * @covers ::getMethod
     * @covers ::getResource
     * @covers ::getQueryParams
     * @covers ::getJsonParams
     */
    public function testDefault(): void
    {
        // Deliberately making a request object so it's guaranteed not to have been set before
        $request = new Request();

        $this->assertEquals(Request::GET, $request->getMethod());
        $this->assertEquals('/', $request->getResource());
        $this->assertEmpty($request->getQueryParams());
        $this->assertEmpty($request->getJsonParams());
    }

    /**
     * @covers ::setMethod
     * @covers ::getMethod
     * @dataProvider dataProviderSetMethod
     * @throws InvalidMethodException
     */
    public function testSetMethod(string $method, ?string $expectException = null): void
    {
        if ($expectException !== null) {
            $this->expectException($expectException);
        }

        $request = $this->makeRequest();
        $returnedRequest = $request->setMethod($method);

        $this->assertInstanceOf(Request::class, $returnedRequest);
        $this->assertEquals($request, $returnedRequest);
        $this->assertEquals($method, $returnedRequest->getMethod());
    }

    /**
     * @covers ::setResource
     * @covers ::getResource
     * @dataProvider dataProviderSetResource
     * @throws InvalidResourceException
     */
    public function testSetResource(string $value, ?string $expectedValue, ?string $expectException = null): void
    {
        if ($expectException !== null) {
            $this->expectException($expectException);
        }

        $request = $this->makeRequest();
        $returnedRequest = $request->setResource($value);

        $this->assertInstanceOf(Request::class, $returnedRequest);
        $this->assertEquals($request, $returnedRequest);
        $this->assertEquals($expectedValue, $returnedRequest->getResource());
    }

    /**
     * @covers ::setQueryParams
     * @covers ::getQueryParams
     * @covers ::setJsonParams
     * @covers ::getJsonParams
     * @dataProvider dataProviderParams
     * @param string|array $value
     */
    public function testParams(string $var, $value): void
    {
        $request = $this->makeRequest();
        $request->{'set' . $var}($value);
        $this->assertEquals($value, $request->{'get' . $var}());
    }

    public function dataProviderSetMethod(): array
    {
        return [
            Request::GET => [Request::GET],
            Request::POST => [Request::POST],
            Request::PUT => [Request::PUT],
            Request::PATCH => [Request::PATCH],
            Request::DELETE => [Request::DELETE],
            'invalid method' => ['foobar', InvalidMethodException::class],
        ];
    }

    public function dataProviderParams(): array
    {
        $faker = Factory::create();

        return [
            'queryParams' => ['QueryParams', [$faker->word => $faker->word]],
            'jsonParams' => ['JsonParams', [$faker->word => $faker->word]],
        ];
    }

    public function dataProviderSetResource(): array
    {
        return [
            'leading and trailing slashes' => ['/foo/bar/', 'foo/bar'],
            'dot' => ['foo.bar', 'foo.bar'],
            'dashes' => ['foo-bar', 'foo-bar'],
            'full url is invalid' => ['http://foo.bar/baz/', null, InvalidResourceException::class],
            'characters other than words, slashes, dots are invalid' => ['foo:bar', null, InvalidResourceException::class],
        ];
    }
}
