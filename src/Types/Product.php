<?php

namespace Dropcart\Api\Types;

use Carbon\Carbon;

class Product extends TypeAbstract
{
    /** @var string */
    protected $ean = '';
    /** @var string  */
    protected $sku = '';
    /** @var bool */
    protected $digital = false;
    /** @var Translations */
    protected $translations;
    /** @var Carbon|null */
    protected $dateAvailable;
    /** @var Categories */
    protected $categories;
    /** @var string */
    protected $brand = '';
    /** @var Attributes */
    protected $attributes;
    /** @var array */
    protected $related = [];
    /** @var array */
    protected $images = [];

    public function getJsonParams(): array
    {
        return [
            'ean' => $this->ean,
            'sku' => $this->sku,
            'digital' => $this->digital,
            'translations' => $this->translations ? $this->translations->getJsonParams() : [],
            'date_available' => $this->dateAvailable ? $this->dateAvailable->format('YYYY-MM-DD') : null,
            'categories' => $this->categories ? $this->categories->getJsonParams() : [],
            'brand' => $this->brand,
            'attributes' => $this->attributes ? $this->attributes->getJsonParams() : [],
            'related' => $this->related,
            'images' => $this->images,
        ];
    }

    /**
     * @param string $ean
     */
    public function setEan(string $ean): void
    {
        $this->ean = $ean;
    }

    /**
     * @param string $sku
     */
    public function setSku(string $sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @param bool $digital
     */
    public function setDigital(bool $digital): void
    {
        $this->digital = $digital;
    }

    /**
     * @param array $translations
     */
    public function setTranslations(array $translations): void
    {
        $this->translations = new Translations($translations);
    }

    /**
     * @param Carbon|null $dateAvailable
     */
    public function setDateAvailable(?Carbon $dateAvailable): void
    {
        $this->dateAvailable = $dateAvailable;
    }

    /**
     * @param array $categories
     */
    public function setCategories(array $categories): void
    {
        $this->categories = new Categories($categories);
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes): void
    {
        $this->attributes = new Attributes($attributes);
    }

    /**
     * @param array $related
     */
    public function setRelated(array $related): void
    {
        $this->related = array_map('strval', $related);
    }

    /**
     * @param array $images
     */
    public function setImages(array $images): void
    {
        $this->images = [];

        foreach ($images as $image) {
            $this->images[] = [
                'file_name' => (string)$image['file_name'],
                'mime_type' => (string)$image['mime_type'],
                'data' => (string)$image['data'],
            ];
        }
    }
}
