<?php

namespace Dropcart\Api\Types;

class Attributes extends TypeAbstract
{
    protected $data = [];

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct(array $data = [])
    {
        $this->data = array_map([$this, 'processData'], $data);
    }

    protected function processData(array $data): array
    {
        return [
            'locale' => (string)$data['locale'],
            'attributes' => $this->castAssocArray((array)$data['attributes']),
        ];
    }

    public function getJsonParams(): array
    {
        return $this->data;
    }

    protected function castAssocArray(array $attributes): array
    {
        $result = [];

        foreach ($attributes as $label => $value) {
            $result[(string)$label] = (string)$value;
        }

        return $result;
    }
}