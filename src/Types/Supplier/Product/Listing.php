<?php

namespace Dropcart\Api\Types\Supplier\Product;

use Dropcart\Api\Types\Listing as BaseListing;

class Listing extends BaseListing
{
    protected $categoryIds = [];
    protected $brandIds = [];
    protected $exclusiveOnly;

    /**
     * @param array $categoryIds
     * @return self
     */
    public function setCategoryIds(array $categoryIds): self
    {
        $this->categoryIds = $categoryIds;
        return $this;
    }

    /**
     * @param array $brandIds
     * @return self
     */
    public function setBrandIds(array $brandIds): self
    {
        $this->brandIds = $brandIds;
        return $this;
    }

    /**
     * @param bool|null $exclusiveOnly
     * @return self
     */
    public function setExclusiveOnly(?bool $exclusiveOnly): self
    {
        $this->exclusiveOnly = $exclusiveOnly;
        return $this;
    }

    public function getQueryParams(): array
    {
        return parent::getQueryParams() + $this->filter([
                'category_ids' => json_encode($this->categoryIds),
                'brand_ids' => json_encode($this->brandIds),
                'exclusive_only' => $this->exclusiveOnly,
            ]);
    }
}