<?php

namespace Dropcart\Api\Types\Supplier\Product;

use Dropcart\Api\Types\Product as BaseProduct;

class Product extends BaseProduct
{
    /** @var array */
    protected $vat = [];
    /** @var float */
    protected $priceExVat = 0.0;
    /** @var float */
    protected $minPriceExVat = 0.0;
    /** @var float */
    protected $maxPriceExVat = 0.0;
    /** @var int */
    protected $stock = 0;
    /** @var bool */
    protected $exclusive = false;
    /** @var array */
    protected $shipping = [];

    public function getJsonParams(): array
    {
        return parent::getJsonParams() + [
                'vat' => $this->vat,
                'price_ex_vat' => $this->priceExVat,
                'min_price_ex_vat' => $this->minPriceExVat,
                'max_price_ex_vat' => $this->maxPriceExVat,
                'stock' => $this->stock,
                'exclusive' => $this->exclusive,
                'shipping' => $this->shipping,
            ];
    }

    /**
     * @param array $vat
     */
    public function setVat(array $vat): void
    {
        $this->vat = [
            'countryCode' => (string)$vat['countryCode'],
            'percentage' => (int)$vat['percentage'],
        ];
    }

    /**
     * @param float $priceExVat
     */
    public function setPriceExVat(float $priceExVat): void
    {
        $this->priceExVat = $priceExVat;
    }

    /**
     * @param float $minPriceExVat
     */
    public function setMinPriceExVat(float $minPriceExVat): void
    {
        $this->minPriceExVat = $minPriceExVat;
    }

    /**
     * @param float $maxPriceExVat
     */
    public function setMaxPriceExVat(float $maxPriceExVat): void
    {
        $this->maxPriceExVat = $maxPriceExVat;
    }

    /**
     * @param int $stock
     */
    public function setStock(int $stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @param bool $exclusive
     */
    public function setExclusive(bool $exclusive): void
    {
        $this->exclusive = $exclusive;
    }

    /**
     * @param array $shipping
     */
    public function setShipping(array $shipping): void
    {
        $this->shipping = [];

        foreach ($shipping as $destination) {
            $this->shipping[] = [
                'countryCode' => (string)$destination['countryCode'],
                'rate'  => (float)$destination['rate'],
                'days' => (int)$destination['days'],
            ];
        }
    }
}