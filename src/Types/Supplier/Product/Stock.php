<?php

namespace Dropcart\Api\Types\Supplier\Product;

use Dropcart\Api\Exceptions\Types\RequiredException;
use Dropcart\Api\Types\TypeAbstract;

class Stock extends TypeAbstract
{
    /** @var array */
    protected $data = [];

    /**
     * Stock constructor.
     *
     * @param array $data
     * @throws RequiredException
     * @noinspection PhpMissingParentConstructorInspection
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $row) {
            $this->add($row);
        }
    }

    public function getJsonParams(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @throws RequiredException
     */
    public function add(array $data)
    {
        if (empty($data['id']) && empty($data['ean'])) {
            throw new RequiredException('At least one ID or EAN is required');
        }

        if (!isset($data['stock'])) {
            throw new RequiredException('Stock is required');
        }

        $this->data[] = [
            'id' => empty($data['id']) ? 0 : (int)$data['id'],
            'ean' => empty($data['ean']) ? '' : (string)$data['ean'],
            'stock' => $data['stock'],
        ];
    }
}