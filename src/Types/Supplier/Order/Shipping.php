<?php

namespace Dropcart\Api\Types\Supplier\Order;

use Carbon\Carbon;
use Dropcart\Api\Types\TypeAbstract;

class Shipping extends TypeAbstract
{
    /** @var int */
    protected $courierId = 0;
    /** @var string */
    protected $trackingCode = '';
    /** @var Carbon */
    protected $deliveredOn;

    public function getJsonParams(): array
    {
        return [
            'courier_id' => $this->courierId,
            'tracking_code' => $this->trackingCode,
            'delivered_on' => $this->deliveredOn->format('Y-m-d'),
        ];
    }

    /**
     * @param int $courierId
     * @return self
     */
    public function setCourierId(int $courierId): self
    {
        $this->courierId = $courierId;
        return $this;
    }

    /**
     * @param string $trackingCode
     * @return self
     */
    public function setTrackingCode(string $trackingCode): self
    {
        $this->trackingCode = $trackingCode;
        return $this;
    }

    /**
     * @param Carbon $deliveredOn
     * @return self
     */
    public function setDeliveredOn(Carbon $deliveredOn): self
    {
        $this->deliveredOn = $deliveredOn;
        return $this;
    }
}