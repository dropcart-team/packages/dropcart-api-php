<?php

namespace Dropcart\Api\Types\Supplier\Order;

use Dropcart\Api\Types\Listing as BaseListing;

class Listing extends BaseListing
{
    protected $orderStatusIds = [];
    protected $productStatusId = 0;

    /**
     * @param array $orderStatusIds
     */
    public function setOrderStatusIds(array $orderStatusIds): void
    {
        $this->orderStatusIds = $orderStatusIds;
    }

    /**
     * @param int $productStatusId
     */
    public function setProductStatusId(int $productStatusId): void
    {
        $this->productStatusId = $productStatusId;
    }

    public function getQueryParams(): array
    {
        return parent::getQueryParams() + $this->filter([
                'order_status_ids' => json_encode($this->orderStatusIds),
                'product_status_id' => $this->productStatusId === 0 ? null : $this->productStatusId,
            ]);
    }
}
