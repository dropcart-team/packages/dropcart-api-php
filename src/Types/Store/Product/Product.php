<?php

namespace Dropcart\Api\Types\Store\Product;

use Dropcart\Api\Types\Translations;
use Dropcart\Api\Types\TypeAbstract;

class Product extends TypeAbstract
{
    /** @var int */
    protected $marginRuleId = 0;
    /** @var int */
    protected $minimumQuantity = 0;
    /** @var Translations */
    protected $translations;

    public function getJsonParams(): array
    {
        return [
            'margin_rule_id' => $this->marginRuleId,
            'minimum_quantity' => $this->minimumQuantity,
            'translations' => isset($this->translations) ? $this->translations->getJsonParams() : null,
        ];
    }

    /**
     * @param int $marginRuleId
     * @return self
     */
    public function setMarginRuleId(int $marginRuleId): self
    {
        $this->marginRuleId = $marginRuleId;
        return $this;
    }

    /**
     * @param int $minimumQuantity
     * @return self
     */
    public function setMinimumQuantity(int $minimumQuantity): self
    {
        $this->minimumQuantity = $minimumQuantity;
        return $this;
    }

    /**
     * @param Translations $translations
     * @return self
     */
    public function setTranslations(Translations $translations): self
    {
        $this->translations = $translations;
        return $this;
    }
}