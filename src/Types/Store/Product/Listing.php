<?php

namespace Dropcart\Api\Types\Store\Product;

use Dropcart\Api\Types\Listing as BaseListing;

class Listing extends BaseListing
{
    protected $unavailableOnly = false;
    protected $eans = [];
    protected $skus = [];
    protected $categoryIds = [];
    protected $brandIds = [];

    public function getQueryParams(): array
    {
        return parent::getQueryParams() + [
                'show_unavailable_only' => $this->unavailableOnly,
                'eans' => json_encode($this->eans),
                'skus' => json_encode($this->skus),
                'category_ids' => json_encode($this->categoryIds),
                'brand_ids' => json_encode($this->brandIds),
            ];
    }

    /**
     * @param bool $unavailableOnly
     */
    public function setUnavailableOnly(bool $unavailableOnly): void
    {
        $this->unavailableOnly = $unavailableOnly;
    }

    /**
     * @param array $eans
     */
    public function setEans(array $eans): void
    {
        $this->eans = $eans;
    }

    /**
     * @param array $skus
     */
    public function setSkus(array $skus): void
    {
        $this->skus = $skus;
    }

    /**
     * @param array $categoryIds
     * @return self
     */
    public function setCategoryIds(array $categoryIds): self
    {
        $this->categoryIds = $categoryIds;
        return $this;
    }

    /**
     * @param array $brandIds
     * @return self
     */
    public function setBrandIds(array $brandIds): self
    {
        $this->brandIds = $brandIds;
        return $this;
    }
}