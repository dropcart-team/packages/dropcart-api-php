<?php

namespace Dropcart\Api\Types;

abstract class TypeAbstract
{
    public function __construct(array $data = [])
    {
        foreach($data as $property => $value) {
            $setProperty = 'set' . str_replace('_', '', $property);
            if (method_exists($this, $setProperty)) {
                $this->{$setProperty}($value);
            }
        }
    }

    public function getQueryParams(): array
    {
        return [];
    }

    public function getJsonParams(): array
    {
        return [];
    }

    protected function filter(array $data): array
    {
        return array_filter($data, function ($value) {
            return $value !== null && $value !== '[]' && $value !== [];
        });
    }
}