<?php

namespace Dropcart\Api\Types;

class Categories extends TypeAbstract
{
    protected $fields = [
        'locale',
        'name',
    ];

    protected $data = [];

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct(array $data = [])
    {
        $this->data = array_map([$this, 'processTranslations'], $data);
    }

    protected function processTranslations(array $translations): array
    {
        $processed = [];

        foreach ($translations as $translation) {
            $processedTranslation = [];

            foreach ($this->fields as $field) {
                $processedTranslation[$field] = isset($translation[$field]) ? (string)$translation[$field] : '';
            }

            $processed[] = $processedTranslation;
        }

        return $processed;
    }

    public function getJsonParams(): array
    {
        return $this->data;
    }
}