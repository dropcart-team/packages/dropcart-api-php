<?php

namespace Dropcart\Api\Types;

class Translations extends TypeAbstract
{
    protected $fields = [
        'locale',
        'name',
        'description',
        'meta_description',
    ];

    protected $data = [];

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct(array $data = [])
    {
        $this->data = array_map([$this, 'processTranslation'], $data);
    }

    protected function processTranslation(array $translation): array
    {
        $processed = [];

        foreach ($this->fields as $field) {
            $processed[$field] = isset($translation[$field]) ? (string)$translation[$field] : '';
        }

        return $processed;
    }

    public function getJsonParams(): array
    {
        return $this->data;
    }
}