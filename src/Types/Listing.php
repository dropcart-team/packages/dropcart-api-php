<?php

namespace Dropcart\Api\Types;

class Listing extends TypeAbstract
{
    protected $page;
    protected $per_page;
    protected $query;

    /**
     * @param int|null $page
     * @return self
     */
    public function setPage(?int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @param int|null $per_page
     * @return self
     */
    public function setPerPage(?int $per_page): self
    {
        $this->per_page = $per_page;
        return $this;
    }

    /**
     * @param string|null $query
     * @return self
     */
    public function setQuery(?string $query): self
    {
        $this->query = $query;
        return $this;
    }

    public function getQueryParams(): array
    {
        return $this->filter([
            'page' => $this->page,
            'per_page' => $this->per_page,
            'query' => $this->query,
        ]);
    }
}