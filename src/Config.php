<?php

namespace Dropcart\Api;

use Dropcart\Api\Exceptions\Config\{InsecureUriException, InvalidApiVersionException, MalformedUriException};

class Config
{
    const API_URI = 'https://rest-api.dropcart.nl';
    const API_VERSION = 'v4';

    const ENV_API_SECURE = 'DROPCART_API_SECURE';
    const ENV_API_URI = 'DROPCART_API_URI';
    const ENV_API_VERSION = 'DROPCART_API_VERSION';

    private $apiSecure = true;
    private $apiUri = self::API_URI;
    private $apiVersion = self::API_VERSION;

    /**
     * API configuration constructor.
     *
     * @throws InsecureUriException
     * @throws InvalidApiVersionException
     * @throws MalformedUriException
     */
    public function __construct()
    {
        $apiSecure = getenv(self::ENV_API_SECURE, true);
        $apiUri = getenv(self::ENV_API_URI, true);
        $apiVersion = getenv(self::ENV_API_VERSION, true);

        $this->setApiSecure($apiSecure === false ? true : (bool)$apiSecure);
        $this->setApiUri($apiUri === false ? self::API_URI : $apiUri);
        $this->setApiVersion($apiVersion === false ? self::API_VERSION : $apiVersion);
    }

    /**
     * @param bool $apiSecure
     * @return $this
     */
    public function setApiSecure(bool $apiSecure): self
    {
        $this->apiSecure = $apiSecure;
        return $this;
    }

    /**
     * @param string $apiUri
     * @return $this
     * @throws InsecureUriException
     * @throws MalformedUriException
     */
    public function setApiUri(string $apiUri): self
    {
        // this regex won't validate FQDNs entirely but is sufficient for this case
        if (!preg_match('#^http(s?)://\w+([-.]\w+)*/?$#', $apiUri, $matches)) {
            throw new MalformedUriException();
        } elseif ($this->apiSecure && empty($matches[1])) {
            throw new InsecureUriException();
        }

        $this->apiUri = rtrim($apiUri, '/');
        return $this;
    }

    /**
     * @param string $apiVersion
     * @return $this
     * @throws InvalidApiVersionException
     */
    public function setApiVersion(string $apiVersion): self
    {
        if (!preg_match('#^v\d+(\.\d+)*$#', $apiVersion)) {
            throw new InvalidApiVersionException();
        }

        $this->apiVersion = $apiVersion;
        return $this;
    }

    /**
     * @return string
     */
    public function getBaseUri()
    {
        return $this->apiUri . '/' . $this->apiVersion . '/';
    }
}