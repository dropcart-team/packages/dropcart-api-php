<?php

namespace Dropcart\Api\Resources;

class CategoryCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new CategoryResource();
    }
}