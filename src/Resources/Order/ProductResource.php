<?php

namespace Dropcart\Api\Resources\Order;

use Dropcart\Api\Exceptions\Resources\InvalidDataException;
use Dropcart\Api\Resources\ProductResource as GlobalProductResource;
use Dropcart\Api\Resources\ResourceAbstract;
use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Resources\ShippingDetailsResource;

class ProductResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var int */
    protected $statusId = 0;
    /** @var int */
    protected $productId = 0;
    /** @var int */
    protected $quantity = 0;
    /** @var int */
    protected $taxRuleId = 0;
    /** @var string */
    protected $taxRate = '';
    /** @var GlobalProductResource */
    protected $product;
    /** @var ShippingDetailsResource */
    protected $shippingDetails;
    /** @var float */
    protected $totalPriceEx = 0.0;
    /** @var float */
    protected $totalPriceIn = 0.0;

    /**
     * @param object|null $data
     * @param bool $processOnlyId
     * @return ResourceInterface
     * @throws InvalidDataException
     */
    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->statusId = (int)$data->status_id;
        $this->productId = (int)$data->product_id;
        $this->quantity = (int)$data->quantity;
        $this->taxRuleId = (int)$data->tax_rule_id;
        $this->taxRate = (string)$data->tax_rate;
        $this->product = (new GlobalProductResource())->init($data->product);
        $this->shippingDetails = (new ShippingDetailsResource())->init($data->shipping_details);
        $this->totalPriceEx = (float)$data->subtotals->total_price_ex;
        $this->totalPriceIn = (float)$data->subtotals->total_price_in;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatusId(): int
    {
        return $this->statusId;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getTaxRuleId(): int
    {
        return $this->taxRuleId;
    }

    /**
     * @return string
     */
    public function getTaxRate(): string
    {
        return $this->taxRate;
    }

    /**
     * @return GlobalProductResource
     */
    public function getProduct(): GlobalProductResource
    {
        return $this->product;
    }

    /**
     * @return ShippingDetailsResource
     */
    public function getShippingDetails(): ShippingDetailsResource
    {
        return $this->shippingDetails;
    }

    /**
     * @return float
     */
    public function getTotalPriceEx(): float
    {
        return $this->totalPriceEx;
    }

    /**
     * @return float
     */
    public function getTotalPriceIn(): float
    {
        return $this->totalPriceIn;
    }
}
