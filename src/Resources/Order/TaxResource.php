<?php

namespace Dropcart\Api\Resources\Order;

use Dropcart\Api\Resources\ResourceAbstract;
use Dropcart\Api\Resources\ResourceInterface;

class TaxResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var int */
    protected $countryId = 0;
    /** @var string */
    protected $title = '';
    /** @var string */
    protected $description = '';
    /** @var float */
    protected $rate = 0.0;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->countryId = (int)$data->country_id;
        $this->title = (string)$data->title;
        $this->description = (string)$data->description;
        $this->rate = (float)$data->rate;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCountryId(): int
    {
        return $this->countryId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }
}