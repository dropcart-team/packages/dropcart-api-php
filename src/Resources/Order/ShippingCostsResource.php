<?php

namespace Dropcart\Api\Resources\Order;

use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Resources\ShippingCostsResource as GlobalShippingCostsResource;

class ShippingCostsResource extends GlobalShippingCostsResource
{
    protected $taxRuleId = 0;
    protected $taxRate = 0.0;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null || $processOnlyId) {
            return $this;
        }

        $this->taxRuleId = (int)$data->tax_rule_id;
        $this->taxRate = (float)$data->tax_rate;
        $this->totalPriceEx = (float)$data->total_price_ex;
        $this->totalPriceIn = (float)$data->total_price_in;

        return $this;
    }

    /**
     * @return int
     */
    public function getTaxRuleId(): int
    {
        return $this->taxRuleId;
    }

    /**
     * @return float
     */
    public function getTaxRate(): float
    {
        return $this->taxRate;
    }
}