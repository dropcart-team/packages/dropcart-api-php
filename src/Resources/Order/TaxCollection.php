<?php

namespace Dropcart\Api\Resources\Order;

use Dropcart\Api\Resources\CollectionAbstract;
use Dropcart\Api\Resources\ResourceInterface;

class TaxCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new TaxResource();
    }
}