<?php

namespace Dropcart\Api\Resources;

class CategoryResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var int */
    protected $parentId = 0;
    /** @var TranslationCollection */
    protected $translations;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->parentId = (int)$data->parent_id;
        $this->translations = (new TranslationCollection())->initData((array)$data->translations);

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }

    /**
     * @return TranslationCollection
     */
    public function getTranslations(): TranslationCollection
    {
        return $this->translations;
    }
}