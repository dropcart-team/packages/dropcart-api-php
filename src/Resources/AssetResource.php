<?php

namespace Dropcart\Api\Resources;

class AssetResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var string */
    protected $mimeType = '';
    /** @var string */
    protected $fileName = '';
    /** @var string */
    protected $url = '';
    /** @var string */
    protected $preset = '';
    /** @var array */
    protected $metaData = [];
    /** @var string */
    protected $checksum = '';
    /** @var int */
    protected $parentId = 0;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->mimeType = (string)$data->mime_type;
        $this->fileName = (string)$data->file_name;
        $this->url = (string)$data->url;
        $this->preset = (string)$data->preset;
        $this->metaData = (array)$data->metadata;
        $this->checksum = (string)$data->md5_checksum;
        $this->parentId = (int)$data->parent_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getPreset(): string
    {
        return $this->preset;
    }

    /**
     * @return array
     */
    public function getMetaData(): array
    {
        return $this->metaData;
    }

    /**
     * @return string
     */
    public function getChecksum(): string
    {
        return $this->checksum;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }
}