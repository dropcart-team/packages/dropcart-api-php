<?php

namespace Dropcart\Api\Resources;

class TranslationResource extends ResourceAbstract
{
    /** @var int */
    protected $languageId = 0;
    /** @var string */
    protected $name = '';
    /** @var string */
    protected $description = '';
    /** @var string */
    protected $metaDescription = '';

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null || $processOnlyId) {
            return $this;
        }

        $this->languageId = (int)$data->language_id;
        $this->name = (string)$data->name;
        $this->description = (string)$data->description;
        $this->metaDescription = (string)$data->meta_description;

        return $this;
    }

    /**
     * @return int
     */
    public function getLanguageId(): int
    {
        return $this->languageId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }
}