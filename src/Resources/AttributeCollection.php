<?php

namespace Dropcart\Api\Resources;

class AttributeCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new AttributeResource();
    }
}