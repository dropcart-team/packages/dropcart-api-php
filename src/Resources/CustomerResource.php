<?php

namespace Dropcart\Api\Resources;

class CustomerResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var string */
    protected $firstName = '';
    /** @var string */
    protected $lastName = '';
    /** @var string */
    protected $email = '';
    /** @var string */
    protected $telephone = '';
    /** @var AddressResource */
    protected $shippingAddress;
    /** @var AddressResource */
    protected $billingAddress;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->firstName = (string)$data->first_name;
        $this->lastName = (string)$data->last_name;
        $this->email = (string)$data->email;
        $this->telephone = (string)$data->telephone;
        $this->shippingAddress = (new AddressResource())->init($data->shipping_address);
        $this->billingAddress = (new AddressResource())->init($data->billing_address);

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @return AddressResource
     */
    public function getShippingAddress(): AddressResource
    {
        return $this->shippingAddress;
    }

    /**
     * @return AddressResource
     */
    public function getBillingAddress(): AddressResource
    {
        return $this->billingAddress;
    }
}