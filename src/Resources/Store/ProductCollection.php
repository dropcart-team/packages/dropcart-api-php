<?php

namespace Dropcart\Api\Resources\Store;

use Dropcart\Api\Resources\CollectionAbstract;
use Dropcart\Api\Resources\ResourceInterface;

class ProductCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new ProductResource();
    }
}