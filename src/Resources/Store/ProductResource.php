<?php

namespace Dropcart\Api\Resources\Store;

use Dropcart\Api\Exceptions\Resources\InvalidDataException as InvalidDataExceptionAlias;
use Dropcart\Api\Resources\Product\PriceCollection;
use Dropcart\Api\Resources\ProductResource as GlobalProductResource;
use Dropcart\Api\Resources\ResourceAbstract;
use Dropcart\Api\Resources\ResourceInterface;

class ProductResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var int */
    protected $marginRuleId = 0;
    /** @var int */
    protected $minimumQuantity = 0;
    /** @var GlobalProductResource */
    protected $product;
    /** @var PriceCollection */
    protected $priceDetails;

    /**
     * @param object|null $data
     * @param bool $processOnlyId
     * @return ResourceInterface
     * @throws InvalidDataExceptionAlias
     */
    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->marginRuleId = (int)$data->margin_rule_id;
        $this->minimumQuantity = (int)$data->minimum_quantity;
        $this->product = (new GlobalProductResource())->init($data->product);
        $this->priceDetails = (new PriceCollection())->initData((array)$data->price_details);

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMarginRuleId(): int
    {
        return $this->marginRuleId;
    }

    /**
     * @return int
     */
    public function getMinimumQuantity(): int
    {
        return $this->minimumQuantity;
    }

    /**
     * @return GlobalProductResource
     */
    public function getProduct(): GlobalProductResource
    {
        return $this->product;
    }

    /**
     * @return PriceCollection
     */
    public function getPriceDetails(): PriceCollection
    {
        return $this->priceDetails;
    }
}