<?php

namespace Dropcart\Api\Resources;

use Carbon\Carbon;
use Dropcart\Api\Exceptions\Resources\InvalidDataException;
use Dropcart\Api\Resources\Supplier\ProductCollection;

class ProductResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var string */
    protected $ean = '';
    /** @var string */
    protected $sku = '';
    /** @var bool */
    protected $digital = false;
    /** @var AssetCollection */
    protected $images;
    /** @var TranslationCollection */
    protected $translations;
    /** @var Carbon|null */
    protected $dateAvailable;
    /** @var CategoryCollection */
    protected $categories;
    /** @var BrandResource */
    protected $brand;
    /** @var AttributeCollection */
    protected $attributes;
    /** @var ProductCollection */
    protected $related;

    /**
     * @param object|null $data
     * @return ResourceInterface
     * @throws InvalidDataException
     */
    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->ean = (string)$data->ean;
        $this->sku = (string)$data->sku;
        $this->digital = (bool)$data->digital;
        $this->images = (new AssetCollection())->initData(isset($data->images) ? (array)$data->images : []);
        $this->translations = (new TranslationCollection())->initData((array)$data->translations);
        $this->dateAvailable = empty($data->date_available) ? null : new Carbon($data->date_available);
        $this->categories = (new CategoryCollection())->initData(isset($data->categories) ? (array)$data->categories : []);
        $this->brand = (new BrandResource())->init($data->brand);
        $this->attributes = (new AttributeCollection())->initData(isset($data->attributes) ? (array)$data->attributes : []);
        $this->related = (new ProductCollection())->initData($data->related);

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEan(): string
    {
        return $this->ean;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @return bool
     */
    public function isDigital(): bool
    {
        return $this->digital;
    }

    /**
     * @return TranslationCollection
     */
    public function getTranslations(): TranslationCollection
    {
        return $this->translations;
    }

    /**
     * @return AttributeCollection
     */
    public function getAttributes(): AttributeCollection
    {
        return $this->attributes;
    }

    /**
     * @return BrandResource
     */
    public function getBrand(): BrandResource
    {
        return $this->brand;
    }

    /**
     * @return CategoryCollection
     */
    public function getCategories(): CategoryCollection
    {
        return $this->categories;
    }

    /**
     * @return Carbon|null
     */
    public function getDateAvailable(): ?Carbon
    {
        return $this->dateAvailable;
    }

    /**
     * @return AssetCollection
     */
    public function getImages(): AssetCollection
    {
        return $this->images;
    }

    /**
     * @return ProductCollection
     */
    public function getRelated(): ProductCollection
    {
        return $this->related;
    }
}