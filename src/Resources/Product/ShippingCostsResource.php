<?php

namespace Dropcart\Api\Resources\Product;

use Dropcart\Api\Resources\ResourceAbstract;
use Dropcart\Api\Resources\ResourceInterface;

class ShippingCostsResource extends ResourceAbstract
{
    /** @var float */
    protected $basePriceEx = 0.0;
    /** @var int */
    protected $countryId = 0;
    /** @var int */
    protected $shippingDays = 0;
    /** @var float */
    protected $resellerMarginEx = 0.0;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null || $processOnlyId) {
            return $this;
        }

        $this->basePriceEx = (float)$data->base_price_ex;
        $this->countryId = (int)$data->country_id;
        $this->shippingDays = (int)$data->shipping_days;
        $this->resellerMarginEx = (float)$data->reseller_margin_ex;

        return $this;
    }
}