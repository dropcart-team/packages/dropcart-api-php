<?php

namespace Dropcart\Api\Resources\Product;

use Dropcart\Api\Resources\ResourceAbstract;
use Dropcart\Api\Resources\ResourceInterface;

class TaxResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var int */
    protected $countryId = 0;
    /** @var string */
    protected $rate = '';
    /** @var string */
    protected $title = '';
    /** @var string */
    protected $description = '';
    /** @var float */
    protected $basePriceIn = 0.0;
    /** @var float */
    protected $dropcartMarginIn = 0.0;
    /** @var float */
    protected $resellerMarginIn = 0.0;
    /** @var float */
    protected $paymentFeeIn = 0.0;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->countryId = (int)$data->country_id;
        $this->rate = (string)$data->rate;
        $this->title = (string)$data->title;
        $this->description = (string)$data->description;
        $this->basePriceIn = (float)$data->base_price_in;
        $this->dropcartMarginIn = (float)$data->dropcart_margin_in;
        $this->resellerMarginIn = (float)$data->reseller_margin_in;
        $this->paymentFeeIn = (float)$data->payment_fee_in;

        return $this;
    }
}