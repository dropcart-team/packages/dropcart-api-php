<?php

namespace Dropcart\Api\Resources\Product;

use Dropcart\Api\Resources\CollectionAbstract;
use Dropcart\Api\Resources\ResourceInterface;

class PriceCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new PriceResource();
    }
}