<?php

namespace Dropcart\Api\Resources\Product;

use Dropcart\Api\Resources\Order\TaxResource;
use Dropcart\Api\Resources\ResourceAbstract;
use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Resources\ShippingCostsCollection;

class PriceResource extends ResourceAbstract
{
    /** @var float */
    protected $basePriceEx = 0.0;
    /** @var ShippingCostsCollection */
    protected $shippingCosts;
    /** @var int */
    protected $supplierId = 0;
    /** @var string */
    protected $supplierName = '';
    /** @var int */
    protected $supplierProductId = 0;
    /** @var int */
    protected $supplierStock = 0;
    /** @var float */
    protected $supplierMinPrice = 0.0;
    /** @var float */
    protected $supplierMaxPrice = 0.0;
    /** @var float */
    protected $dropcartMarginEx = 0.0;
    /** @var float */
    protected $paymentFeeEx = 0.0;
    /** @var TaxResource */
    protected $taxRule;
    /** @var float */
    protected $totalPriceEx = 0.0;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null || $processOnlyId) {
            return $this;
        }

        $this->basePriceEx = (float)$data->base_price_ex;
        $this->shippingCosts = (new ShippingCostsCollection())->initData((array)$data->shipping_costs);
        $this->supplierId = (int)$data->supplier_id;
        $this->supplierName = (string)$data->supplier_name;
        $this->supplierProductId = (int)$data->supplier_product_id;
        $this->supplierStock = (int)$data->supplier_stock;
        $this->supplierMinPrice = (float)$data->supplier_min_price;
        $this->supplierMaxPrice = (float)$data->supplier_max_price;
        $this->dropcartMarginEx = (float)$data->dropcart_margin_ex;
        $this->paymentFeeEx = (float)$data->payment_fee_ex;
        $this->taxRule = (new TaxResource())->init($data->tax_rule);
        $this->totalPriceEx = (float)$data->total_price_ex;

        return $this;
    }

    /**
     * @return float
     */
    public function getBasePriceEx(): float
    {
        return $this->basePriceEx;
    }

    /**
     * @return float
     */
    public function getTotalPriceEx(): float
    {
        return $this->totalPriceEx;
    }

    /**
     * @return float
     */
    public function getDropcartMarginEx(): float
    {
        return $this->dropcartMarginEx;
    }

    /**
     * @return float
     */
    public function getPaymentFeeEx(): float
    {
        return $this->paymentFeeEx;
    }

    /**
     * @return ShippingCostsCollection
     */
    public function getShippingCosts(): ShippingCostsCollection
    {
        return $this->shippingCosts;
    }

    /**
     * @return int
     */
    public function getSupplierId(): int
    {
        return $this->supplierId;
    }

    /**
     * @return float
     */
    public function getSupplierMaxPrice(): float
    {
        return $this->supplierMaxPrice;
    }

    /**
     * @return float
     */
    public function getSupplierMinPrice(): float
    {
        return $this->supplierMinPrice;
    }

    /**
     * @return string
     */
    public function getSupplierName(): string
    {
        return $this->supplierName;
    }

    /**
     * @return int
     */
    public function getSupplierProductId(): int
    {
        return $this->supplierProductId;
    }

    /**
     * @return int
     */
    public function getSupplierStock(): int
    {
        return $this->supplierStock;
    }

    /**
     * @return TaxResource
     */
    public function getTaxRule(): TaxResource
    {
        return $this->taxRule;
    }
}