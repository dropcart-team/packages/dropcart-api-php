<?php

namespace Dropcart\Api\Resources;

class ShippingCountryCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new ShippingCountryResource();
    }
}