<?php

namespace Dropcart\Api\Resources;

use Carbon\Carbon;
use Exception;

class ShippingDetailsResource extends ResourceAbstract
{
    /** @var int */
    protected $courierId = 0;
    /** @var string */
    protected $courierName = '';
    /** @var string */
    protected $trackingCode = '';
    /** @var Carbon */
    protected $deliveredOn;

    /**
     * @param object|null $data
     * @param bool $processOnlyId
     * @return ResourceInterface
     * @throws Exception
     */
    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null || $processOnlyId) {
            return $this;
        }

        $this->courierId = (int)$data->courier_id;
        $this->courierName = (string)$data->courier_name;
        $this->trackingCode = (string)$data->tracking_code;
        $this->deliveredOn = new Carbon($data->delivered_on);

        return $this;
    }

    /**
     * @return int
     */
    public function getCourierId(): int
    {
        return $this->courierId;
    }

    /**
     * @return string
     */
    public function getCourierName(): string
    {
        return $this->courierName;
    }

    /**
     * @return string
     */
    public function getTrackingCode(): string
    {
        return $this->trackingCode;
    }

    /**
     * @return Carbon
     */
    public function getDeliveredOn(): Carbon
    {
        return $this->deliveredOn;
    }
}