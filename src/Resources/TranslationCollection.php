<?php

namespace Dropcart\Api\Resources;

class TranslationCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new TranslationResource();
    }
}