<?php

namespace Dropcart\Api\Resources;

class AssetCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new AssetResource();
    }
}