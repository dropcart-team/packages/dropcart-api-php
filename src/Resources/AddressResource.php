<?php

namespace Dropcart\Api\Resources;

class AddressResource extends ResourceAbstract
{
    protected $companyName = '';
    protected $firstName = '';
    protected $lastName = '';
    protected $streetName = '';
    protected $buildingNumber = '';
    protected $secondaryAddress = '';
    protected $city = '';
    protected $postcode = '';
    protected $countryId = 0;
    protected $countryName = '';

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null || $processOnlyId) {
            return $this;
        }

        $this->companyName = (string)$data->company;
        $this->firstName = (string)$data->first_name;
        $this->lastName = (string)$data->last_name;
        $this->streetName = (string)$data->address_1;
        $this->buildingNumber = (string)$data->address_house_nr;
        $this->secondaryAddress = (string)$data->address_2;
        $this->city = (string)$data->city;
        $this->postcode = (string)$data->postcode;
        $this->countryId = (int)$data->country_id;
        $this->countryName = (string)$data->country_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getStreetName(): string
    {
        return $this->streetName;
    }

    /**
     * @return string
     */
    public function getBuildingNumber(): string
    {
        return $this->buildingNumber;
    }

    /**
     * @return string
     */
    public function getSecondaryAddress(): string
    {
        return $this->secondaryAddress;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * @return int
     */
    public function getCountryId(): int
    {
        return $this->countryId;
    }

    /**
     * @return string
     */
    public function getCountryName(): string
    {
        return $this->countryName;
    }
}