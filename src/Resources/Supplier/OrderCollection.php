<?php

namespace Dropcart\Api\Resources\Supplier;

use Dropcart\Api\Resources\CollectionAbstract;
use Dropcart\Api\Resources\ResourceInterface;

class OrderCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new OrderResource();
    }
}