<?php

namespace Dropcart\Api\Resources\Supplier;

use Dropcart\Api\Resources\ProductResource as GlobalProductResource;
use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Resources\ShippingCountryCollection;

class ProductResource implements ResourceInterface
{
    /** @var int */
    protected $id = 0;
    /** @var int */
    protected $taxRuleId = 0;
    /** @var float */
    protected $priceEx = 0.0;
    /** @var float */
    protected $minPrice = 0;
    /** @var float */
    protected $maxPrice = 0;
    /** @var int */
    protected $stockStatusId = 0;
    /** @var int */
    protected $stock = 0;
    /** @var bool */
    protected $exclusive = false;
    /** @var GlobalProductResource */
    protected $product;
    /** @var ShippingCountryCollection */
    protected $shippingCountries;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->taxRuleId = (int)$data->tax_rule_id;
        $this->priceEx = (float)$data->price_ex;
        $this->minPrice = (float)$data->min_price;
        $this->maxPrice = (float)$data->max_price;
        $this->stockStatusId = (int)$data->stock_status_id;
        $this->stock = (int)$data->stock;
        $this->exclusive = (bool)$data->exclusive;
        $this->product = (new GlobalProductResource())->init($data->product);
        $this->shippingCountries = (new ShippingCountryCollection())->initData($data->shipping_countries);

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTaxRuleId(): int
    {
        return $this->taxRuleId;
    }

    /**
     * @return float
     */
    public function getPriceEx(): float
    {
        return $this->priceEx;
    }

    /**
     * @return float
     */
    public function getMinPrice(): float
    {
        return $this->minPrice;
    }

    /**
     * @return float
     */
    public function getMaxPrice(): float
    {
        return $this->maxPrice;
    }

    /**
     * @return int
     */
    public function getStockStatusId(): int
    {
        return $this->stockStatusId;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @return GlobalProductResource
     */
    public function getProduct(): GlobalProductResource
    {
        return $this->product;
    }

    /**
     * @return ShippingCountryCollection
     */
    public function getShippingCountries(): ShippingCountryCollection
    {
        return $this->shippingCountries;
    }

    /**
     * @return bool
     */
    public function isExclusive(): bool
    {
        return $this->exclusive;
    }
}