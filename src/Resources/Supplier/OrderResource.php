<?php

namespace Dropcart\Api\Resources\Supplier;

use Dropcart\Api\Exceptions\Resources\InvalidDataException;
use Dropcart\Api\Resources\CustomerResource;
use Dropcart\Api\Resources\Order\ProductCollection as OrderProductCollection;
use Dropcart\Api\Resources\Order\TaxCollection;
use Dropcart\Api\Resources\ResourceAbstract;
use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Resources\Order\ShippingCostsResource;

class OrderResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var int */
    protected $storeId = 0;
    /** @var string */
    protected $storeName = '';
    /** @var int */
    protected $statusId = 0;
    /** @var int */
    protected $productCount = 0;
    /** @var CustomerResource */
    protected $customer;
    /** @var OrderProductCollection */
    protected $orderProducts;
    /** @var ShippingCostsResource */
    protected $shippingCosts;
    /** @var TaxCollection */
    protected $taxes;
    /** @var float */
    protected $totalPriceEx = 0.0;
    /** @var float */
    protected $totalPriceIn = 0.0;

    /**
     * @param object|null $data
     * @return ResourceInterface
     * @throws InvalidDataException
     */
    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->id = (int)$data->id;

        if ($processOnlyId) {
            return $this;
        }

        $this->storeId = (int)$data->store_id;
        $this->storeName = (string)$data->store_name;
        $this->statusId = (int)$data->status_id;
        $this->productCount = (int)$data->product_count;
        $this->customer = (new CustomerResource())->init($data->customer);
        $this->orderProducts = (new OrderProductCollection())->initData((array)$data->order_products);
        $this->shippingCosts = (new ShippingCostsResource())->init($data->shipping_costs);
        $this->taxes = (new TaxCollection())->initData((array)$data->taxes);
        $this->totalPriceEx = !empty($data->totals) ? (float)$data->totals->total_price_ex : 0.0;
        $this->totalPriceIn = !empty($data->totals) ? (float)$data->totals->total_price_in : 0.0;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStoreId(): int
    {
        return $this->storeId;
    }

    /**
     * @return string
     */
    public function getStoreName(): string
    {
        return $this->storeName;
    }

    /**
     * @return int
     */
    public function getStatusId(): int
    {
        return $this->statusId;
    }

    /**
     * @return int
     */
    public function getProductCount(): int
    {
        return $this->productCount;
    }

    /**
     * @return CustomerResource
     */
    public function getCustomer(): CustomerResource
    {
        return $this->customer;
    }

    /**
     * @return OrderProductCollection
     */
    public function getOrderProducts(): OrderProductCollection
    {
        return $this->orderProducts;
    }

    /**
     * @return ShippingCostsResource
     */
    public function getShippingCosts(): ShippingCostsResource
    {
        return $this->shippingCosts;
    }

    /**
     * @return TaxCollection
     */
    public function getTaxes(): TaxCollection
    {
        return $this->taxes;
    }

    /**
     * @return float
     */
    public function getTotalPriceIn(): float
    {
        return $this->totalPriceIn;
    }

    /**
     * @return float
     */
    public function getTotalPriceEx(): float
    {
        return $this->totalPriceEx;
    }
}