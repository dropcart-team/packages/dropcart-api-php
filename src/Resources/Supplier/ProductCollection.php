<?php

namespace Dropcart\Api\Resources\Supplier;

use Dropcart\Api\Resources\CollectionAbstract;
use Dropcart\Api\Resources\ResourceInterface;

class ProductCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new ProductResource();
    }
}