<?php

namespace Dropcart\Api\Resources;

class ShippingCountryResource extends ResourceAbstract
{
    /** @var int */
    protected $id = 0;
    /** @var string */
    protected $name = '';
    /** @var float */
    protected $costs = 0.0;
    /** @var int */
    protected $days = 0;
    /** @var bool */
    protected $global = false;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null || $processOnlyId) {
            return $this;
        }

        $this->id = (int)$data->country_id;
        $this->name = (string)$data->country_name;
        $this->costs = (double)$data->shipping_costs_ex;
        $this->days = (int)$data->shipping_days;
        $this->global = (bool)$data->global;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getCosts(): float
    {
        return $this->costs;
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        return $this->days;
    }

    /**
     * @return bool
     */
    public function isGlobal(): bool
    {
        return $this->global;
    }
}