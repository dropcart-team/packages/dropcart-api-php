<?php

namespace Dropcart\Api\Resources;

class ShippingCostsCollection extends CollectionAbstract
{
    protected function getResourceClass(): ResourceInterface
    {
        return new ShippingCostsResource();
    }
}