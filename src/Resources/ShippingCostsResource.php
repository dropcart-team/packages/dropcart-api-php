<?php

namespace Dropcart\Api\Resources;

class ShippingCostsResource extends ResourceAbstract
{
    protected $countryId = 0;
    protected $shippingDays = 0;
    protected $basePriceEx = 0.0;
    protected $basePriceIn = 0.0;
    protected $dropcartMarginEx = 0.0;
    protected $dropcartMarginIn = 0.0;
    protected $resellerMarginEx = 0.0;
    protected $resellerMarginIn = 0.0;
    protected $paymentFeeEx = 0.0;
    protected $paymentFeeIn = 0.0;
    protected $totalPriceEx = 0.0;
    protected $totalPriceIn = 0.0;

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null || $processOnlyId) {
            return $this;
        }
        
        $this->countryId = (int)$data->country_id;
        $this->shippingDays = (int)$data->shipping_days;
        $this->basePriceEx = (float)$data->base_price_ex;
        $this->basePriceIn = (float)$data->base_price_in;
        $this->dropcartMarginEx = (float)$data->dropcart_margin_ex;
        $this->dropcartMarginIn = (float)$data->dropcart_margin_in;
        $this->resellerMarginEx = (float)$data->reseller_margin_ex;
        $this->resellerMarginIn = (float)$data->reseller_margin_in;
        $this->paymentFeeEx = (float)$data->payment_fee_ex;
        $this->paymentFeeIn = (float)$data->payment_fee_in;
        $this->totalPriceEx = (float)$data->total_price_ex;
        $this->totalPriceIn = (float)$data->total_price_in;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountryId(): int
    {
        return $this->countryId;
    }

    /**
     * @return int
     */
    public function getShippingDays(): int
    {
        return $this->shippingDays;
    }

    /**
     * @return float
     */
    public function getBasePriceEx(): float
    {
        return $this->basePriceEx;
    }

    /**
     * @return float
     */
    public function getBasePriceIn(): float
    {
        return $this->basePriceIn;
    }

    /**
     * @return float
     */
    public function getDropcartMarginEx(): float
    {
        return $this->dropcartMarginEx;
    }

    /**
     * @return float
     */
    public function getDropcartMarginIn(): float
    {
        return $this->dropcartMarginIn;
    }

    /**
     * @return float
     */
    public function getResellerMarginEx(): float
    {
        return $this->resellerMarginEx;
    }

    /**
     * @return float
     */
    public function getResellerMarginIn(): float
    {
        return $this->resellerMarginIn;
    }

    /**
     * @return float
     */
    public function getPaymentFeeEx(): float
    {
        return $this->paymentFeeEx;
    }

    /**
     * @return float
     */
    public function getPaymentFeeIn(): float
    {
        return $this->paymentFeeIn;
    }

    /**
     * @return float
     */
    public function getTotalPriceEx(): float
    {
        return $this->totalPriceEx;
    }

    /**
     * @return float
     */
    public function getTotalPriceIn(): float
    {
        return $this->totalPriceIn;
    }
}