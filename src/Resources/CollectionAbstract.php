<?php

namespace Dropcart\Api\Resources;

use Countable;
use Dropcart\Api\Exceptions\Resources\InvalidDataException;
use Iterator;

abstract class CollectionAbstract implements ResourceInterface, Iterator, Countable
{
    /** @var int */
    protected $totalCount = 0;
    /** @var int */
    protected $currentPage = 0;
    /** @var int */
    protected $perPage = 0;
    /** @var int */
    protected $lastPage = 0;

    /** @var object[] */
    protected $rawData = [];
    /** @var ResourceInterface[] */
    protected $data = [];

    /**
     * @param object|null $data
     * @param bool $processOnlyId
     * @return ResourceInterface
     * @throws InvalidDataException
     */
    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null) {
            return $this;
        }

        $this->totalCount = (int)$data->total;
        $this->currentPage = (int)$data->current_page;
        $this->perPage = (int)$data->per_page;
        $this->lastPage = (int)$data->last_page;

        return isset($data->data) ? $this->initData((array)$data->data) : $this;
    }

    /**
     * @param object[] $data
     * @return ResourceInterface
     * @throws InvalidDataException
     */
    public function initData(array $data): ResourceInterface
    {
        foreach ($data as $row) {
            if (!is_object($row)) {
                throw new InvalidDataException();
            }
        }

        $this->rawData = $data;
        return $this;
    }

    abstract protected function getResourceClass(): ResourceInterface;

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getLastPage(): int
    {
        return $this->lastPage;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        if (empty($this->data) && !empty($this->rawData)) {
            $this->data = array_map(function ($row) {
                return $this->getResourceClass()->init($row);
            }, $this->rawData);
        }

        return $this->data;
    }

    public function current()
    {
        return $this->getResource(key($this->rawData));
    }

    public function key()
    {
        return key($this->rawData);
    }

    public function next()
    {
        next($this->rawData);
        return $this->getResource(key($this->rawData));
    }

    public function rewind()
    {
        reset($this->rawData);
        reset($this->data);
    }

    public function valid()
    {
        return key($this->rawData) !== null;
    }

    public function count()
    {
        return count($this->rawData);
    }

    protected function getResource(?int $key)
    {
        if ($key === null) {
            return null;
        }

        if (!isset($this->data[$key])) {
            $this->data[$key] = $this->getResourceClass()->init($this->rawData[$key]);
        }

        return $this->data[$key];
    }
}