<?php

namespace Dropcart\Api\Resources;

interface ResourceInterface
{
    public function init(?object $data, bool $processOnlyId = false): ResourceInterface;
}