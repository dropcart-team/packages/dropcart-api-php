<?php

namespace Dropcart\Api\Resources;

class AttributeResource extends ResourceAbstract
{
    protected $languageId = 0;
    protected $attributes = [];

    public function init(?object $data, bool $processOnlyId = false): ResourceInterface
    {
        if ($data === null || $processOnlyId) {
            return $this;
        }

        $this->languageId = (int)$data->language_id;
        $this->attributes = !empty($data->attributes) ? $data->attributes : json_decode($data->json_attributes);

        return $this;
    }

    /**
     * @return int
     */
    public function getLanguageId(): int
    {
        return $this->languageId;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
}