<?php

namespace Dropcart\Api\Endpoints;

use Dropcart\Api\Endpoints\Store\ProductEndpoint;

class StoreEndpoint extends EndpointAbstract
{
    protected $resourcePath = 'store';

    /**
     * @return EndpointAbstract|ProductEndpoint
     */
    public function products()
    {
        return $this->getEndpoint(ProductEndpoint::class);
    }
}