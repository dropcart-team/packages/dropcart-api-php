<?php

namespace Dropcart\Api\Endpoints\Store;

use Dropcart\Api\Endpoints\EndpointAbstract;
use Dropcart\Api\Exceptions\DropcartApiException;
use Dropcart\Api\Request;
use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Resources\Store\ProductCollection;
use Dropcart\Api\Resources\Store\ProductResource;
use Dropcart\Api\Types\Store\Product\Listing as StoreProductListing;
use Dropcart\Api\Types\Store\Product\Product as StoreProduct;
use GuzzleHttp\Exception\GuzzleException;

class ProductEndpoint extends EndpointAbstract
{
    protected $resourcePath = 'products';

    /**
     * List store products
     *
     * @param StoreProductListing $listing
     * @return ResourceInterface
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function list(StoreProductListing $listing) {
        $request = $this->request
            ->reset()
            ->setResource($this->resourcePath)
            ->setQueryParams($listing->getQueryParams());

        return $this->getClient()->request($request, new ProductCollection());
    }

    /**
     * @param int $id
     * @return ResourceInterface|null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function get(int $id)
    {
        $request = $this->request
            ->reset()
            ->setResource($this->resourcePath . '/' . $id);

        return $this->getClient()->request($request, new ProductResource());
    }

    /**
     * @param string $ean
     * @return ResourceInterface
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function getByEan(string $ean)
    {
        $request = $this->request
            ->reset()
            ->setResource($this->resourcePath . '/' . $ean)
            ->setQueryParams(['type' => 'ean']);

        return $this->getClient()->request($request, new ProductResource());
    }

    /**
     * @param int $id
     * @param StoreProduct $product
     * @return null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function update(int $id, StoreProduct $product)
    {
        $request = $this->request
            ->reset()
            ->setMethod(Request::PUT)
            ->setResource($this->resourcePath . '/' . $id)
            ->setJsonParams($product->getJsonParams());

        return $this->getClient()->request($request);
    }

    /**
     * @param string $ean
     * @param StoreProduct $product
     * @return null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function updateByEan(string $ean, StoreProduct $product)
    {
        $request = $this->request
            ->reset()
            ->setMethod(Request::PUT)
            ->setResource($this->resourcePath . '/' . $ean)
            ->setJsonParams($product->getJsonParams() + [
                    'type' => 'ean',
                ]);

        return $this->getClient()->request($request);
    }
}