<?php

namespace Dropcart\Api\Endpoints;

use Dropcart\Api\Endpoints\Supplier\OrderEndpoint;
use Dropcart\Api\Endpoints\Supplier\ProductEndpoint;

class SupplierEndpoint extends EndpointAbstract
{
    protected $resourcePath = 'supplier';

    /**
     * @return EndpointAbstract|ProductEndpoint
     */
    public function products()
    {
        return $this->getEndpoint(ProductEndpoint::class);
    }

    /**
     * @return EndpointAbstract|OrderEndpoint
     */
    public function orders()
    {
        return $this->getEndpoint(OrderEndpoint::class);
    }
}