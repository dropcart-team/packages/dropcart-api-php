<?php

namespace Dropcart\Api\Endpoints;

use Dropcart\Api\Client;
use Dropcart\Api\Request;
use Dropcart\Api\Traits\GetEndpoint;

abstract class EndpointAbstract
{
    use GetEndpoint;

    /** @var Client */
    protected $client;

    /** @var Request */
    protected $request;

    public function __construct(Client $client, string $resourcePathPrefix = '', ?Request $request = null)
    {
        $this->client = $client;
        $this->resourcePath = trim($resourcePathPrefix . '/' . $this->resourcePath, '/');
        $this->request = $request === null ? new Request() : $request;
    }

    protected function getClient(): Client
    {
        return $this->client;
    }
}