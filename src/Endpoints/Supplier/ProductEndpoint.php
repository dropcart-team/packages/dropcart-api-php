<?php

namespace Dropcart\Api\Endpoints\Supplier;

use Dropcart\Api\Endpoints\EndpointAbstract;
use Dropcart\Api\Exceptions\DropcartApiException;
use Dropcart\Api\Request;
use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Resources\Supplier\ProductCollection;
use Dropcart\Api\Resources\Supplier\ProductResource;
use Dropcart\Api\Types\Supplier\Product\Listing as SupplierProductListing;
use Dropcart\Api\Types\Supplier\Product\Product as SupplierProduct;
use Dropcart\Api\Types\Supplier\Product\Stock as SupplierProductStock;
use GuzzleHttp\Exception\GuzzleException;

class ProductEndpoint extends EndpointAbstract
{
    protected $resourcePath = 'products';

    /**
     * List supplier products
     *
     * @param SupplierProductListing $listing
     * @return ResourceInterface
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function list(SupplierProductListing $listing)
    {
        $request = $this->request
            ->reset()
            ->setResource($this->resourcePath)
            ->setQueryParams($listing->getQueryParams());

        return $this->getClient()->request($request, new ProductCollection());
    }

    /**
     * Get supplier product by identifier
     *
     * @param int $id Supplier product identifier
     * @return ResourceInterface
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function get(int $id)
    {
        $request = $this->request
            ->reset()
            ->setResource($this->resourcePath . '/' . $id);

        return $this->getClient()->request($request, new ProductResource());
    }

    /**
     * Get supplier product by product EAN
     *
     * @param string $ean Product EAN
     * @return ResourceInterface
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function getByEan(string $ean)
    {
        $request = $this->request
            ->reset()
            ->setResource($this->resourcePath . '/' . $ean)
            ->setQueryParams(['type' => 'ean']);

        return $this->getClient()->request($request, new ProductResource());
    }

    /**
     * Add supplier product
     *
     * @param SupplierProduct $supplierProduct
     * @return ResourceInterface
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function add(SupplierProduct $supplierProduct)
    {
        $request = $this->request
            ->reset()
            ->setMethod(Request::POST)
            ->setResource($this->resourcePath)
            ->setJsonParams($supplierProduct->getJsonParams());

        return $this->getClient()->request($request, new ProductResource());
    }

    /**
     * Update supplier product by identifier
     *
     * @param int $id Supplier product identifier
     * @param SupplierProduct $supplierProduct
     * @return null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function update(int $id, SupplierProduct $supplierProduct)
    {
        $request = $this->request
            ->reset()
            ->setMethod(Request::PUT)
            ->setResource($this->resourcePath . '/' . $id)
            ->setJsonParams($supplierProduct->getJsonParams());

        return $this->getClient()->request($request);
    }

    /**
     * Update supplier product by product EAN
     *
     * @param string $ean
     * @param SupplierProduct $supplierProduct
     * @return null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function updateByEan(string $ean, SupplierProduct $supplierProduct)
    {
        $request = $this->request
            ->reset()
            ->setMethod(Request::PUT)
            ->setResource($this->resourcePath . '/' . $ean)
            ->setJsonParams(['type' => 'ean'] + $supplierProduct->getJsonParams());

        return $this->getClient()->request($request);
    }

    /**
     * Remove supplier product by identifier
     *
     * @param int $id Supplier product identifier
     * @return ResourceInterface|null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function remove(int $id)
    {
        $request = $this->request
            ->reset()
            ->setMethod(Request::DELETE)
            ->setResource($this->resourcePath . '/' . $id);

        return $this->getClient()->request($request);
    }

    /**
     * Remove supplier product by product EAN
     *
     * @param string $ean Product EAN
     * @return ResourceInterface|null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function removeByEan(string $ean)
    {
        $request = $this->request
            ->reset()
            ->setMethod(Request::DELETE)
            ->setResource($this->resourcePath . '/' . $ean)
            ->setQueryParams(['type' => 'ean']);

        return $this->getClient()->request($request);
    }

    /**
     * Update stock of one or more supplier products
     *
     * @param SupplierProductStock $supplierProductStock
     * @return null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function updateStock(SupplierProductStock $supplierProductStock)
    {
        $request = $this->request
            ->reset()
            ->setMethod(Request::PATCH)
            ->setResource($this->resourcePath . '/stock')
            ->setJsonParams(['data' => $supplierProductStock->getJsonParams()]);

        return $this->getClient()->request($request);
    }
}