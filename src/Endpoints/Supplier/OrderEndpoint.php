<?php

namespace Dropcart\Api\Endpoints\Supplier;

use Dropcart\Api\Endpoints\EndpointAbstract;
use Dropcart\Api\Exceptions\DropcartApiException;
use Dropcart\Api\Request;
use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Resources\Supplier\{OrderCollection, OrderResource};
use Dropcart\Api\Types\Supplier\Order\Listing as SupplierOrderListing;
use Dropcart\Api\Types\Supplier\Order\Shipping;
use GuzzleHttp\Exception\GuzzleException;

class OrderEndpoint extends EndpointAbstract
{
    protected $resourcePath = 'orders';

    /**
     * @param SupplierOrderListing $listing
     * @return ResourceInterface|null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function list(SupplierOrderListing $listing)
    {
        $request = $this->request
            ->reset()
            ->setResource($this->resourcePath)
            ->setQueryParams($listing->getQueryParams());

        return $this->getClient()->request($request, new OrderCollection());
    }

    /**
     * @param int $id
     * @return ResourceInterface|null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function get(int $id)
    {
        $request = $this->request
            ->reset()
            ->setResource($this->resourcePath . '/' . $id);

        return $this->getClient()->request($request, new OrderResource());
    }

    /**
     * @param int $orderId
     * @param int $productId
     * @param Shipping $shipping
     * @return ResourceInterface|null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function updateProductShipping(int $orderId, int $productId, Shipping $shipping)
    {
        $request = $this->request
            ->reset()
            ->setMethod(Request::PATCH)
            ->setResource($this->resourcePath . '/' . $orderId . '/shipping/' . $productId)
            ->setJsonParams($shipping->getJsonParams());

        return $this->getClient()->request($request);
    }
}