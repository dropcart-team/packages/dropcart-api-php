<?php

namespace Dropcart\Api;

use Dropcart\Api\Exceptions\Request\InvalidMethodException;
use Dropcart\Api\Exceptions\Request\InvalidResourceException;

class Request
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const PATCH = 'PATCH';
    const DELETE = 'DELETE';

    protected $allowedMethods = [
        self::GET => true,
        self::POST => true,
        self::PUT => true,
        self::PATCH => true,
        self::DELETE => true,
    ];

    /** @var string */
    protected $method = self::GET;

    /** @var string */
    protected $resource = '/';

    /** @var array */
    protected $queryParams = [];

    /** @var array */
    protected $jsonParams = [];

    public function reset()
    {
        $this->method = self::GET;
        $this->resource = '/';
        $this->queryParams = [];
        $this->jsonParams = [];

        return $this;
    }

    /**
     * @param string $method
     * @return self
     * @throws InvalidMethodException
     */
    public function setMethod(string $method): self
    {
        $method = strtoupper($method);
        if (!isset($this->allowedMethods[$method])) {
            throw new InvalidMethodException();
        }

        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $resource
     * @return self
     * @throws InvalidResourceException
     */
    public function setResource(string $resource): self
    {
        if (!preg_match('#^/?[\w.-]+(/[\w.-]+)*/?$#', $resource)) {
            throw new InvalidResourceException();
        }

        $this->resource = trim($resource, '/');
        return $this;
    }

    /**
     * @return string
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * @param array $queryParams
     * @return self
     */
    public function setQueryParams(array $queryParams): self
    {
        $this->queryParams = $queryParams;
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @param array $jsonParams
     * @return self
     */
    public function setJsonParams(array $jsonParams): self
    {
        $this->jsonParams = $jsonParams;
        return $this;
    }

    /**
     * @return array
     */
    public function getJsonParams(): array
    {
        return $this->jsonParams;
    }
}