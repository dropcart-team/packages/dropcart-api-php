<?php

namespace Dropcart\Api;

use Dropcart\Api\Endpoints\EndpointAbstract;
use Dropcart\Api\Endpoints\StoreEndpoint;
use Dropcart\Api\Endpoints\SupplierEndpoint;
use Dropcart\Api\Exceptions\DropcartApiException;
use Dropcart\Api\Resources\ResourceInterface;
use Dropcart\Api\Exceptions\Response\{DuplicateModelException,
    ForbiddenException,
    InternalServerErrorException,
    ModelNotFoundException,
    PreconditionFailureException,
    UnauthorizedException,
    UnknownResponseException};
use Dropcart\Api\Traits\GetEndpoint;
use GuzzleHttp\ClientInterface as GuzzleHttpClientInterface;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions as GuzzleRequestOptions;

class Client
{
    use GetEndpoint;

    /** @var Token */
    protected $token;

    /** @var Config */
    protected $config;

    /** @var GuzzleHttpClientInterface */
    protected $httpClient;

    public function __construct(Token $token = null, Config $config = null, GuzzleHttpClientInterface $httpClient = null)
    {
        $this->setToken($token ?: new Token());
        $this->setConfig($config ?: new Config());
        $this->setHttpClient($httpClient ?: new GuzzleHttpClient());
    }

    public function setToken(Token $token)
    {
        $this->token = $token;
    }

    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    public function setHttpClient(GuzzleHttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param Request $request
     * @param ResourceInterface $resource
     * @return ResourceInterface|null
     * @throws DropcartApiException
     * @throws GuzzleException
     */
    public function request(Request $request, ?ResourceInterface $resource = null): ?ResourceInterface
    {
        $response = $this->httpClient->request(
            $request->getMethod(),
            $this->config->getBaseUri() . $request->getResource(),
            [
                GuzzleRequestOptions::HTTP_ERRORS => false,
                GuzzleRequestOptions::HEADERS => [
                    'Authorization' => 'Bearer ' . $this->token->get(),
                ],
                GuzzleRequestOptions::QUERY => $request->getQueryParams() ?: null,
                GuzzleRequestOptions::JSON => $request->getJsonParams() ?: null,
            ]
        );

        switch ($response->getStatusCode()) {
            case 200;
                return $resource === null ? null : $resource->init(json_decode((string)$response->getBody()));
            case 201:
                return $resource->init(json_decode((string)$response->getBody()), true);
            case 204:
                return null;
            case 401:
                throw new UnauthorizedException();
            case 403:
                throw new ForbiddenException();
            case 404:
                throw new ModelNotFoundException();
            case 409:
                throw new DuplicateModelException();
            case 422:
                throw new PreconditionFailureException($response->getBody());
            case 500:
                throw new InternalServerErrorException();
            default:
                throw new UnknownResponseException($response->getReasonPhrase());
        }
    }

    /**
     * @return EndpointAbstract|SupplierEndpoint
     */
    public function supplier()
    {
        return $this->getEndpoint(SupplierEndpoint::class);
    }

    /**
     * @return EndpointAbstract|StoreEndpoint
     */
    public function store()
    {
        return $this->getEndpoint(StoreEndpoint::class);
    }
}
