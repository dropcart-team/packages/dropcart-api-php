<?php

namespace Dropcart\Api;

use Carbon\CarbonImmutable;
use Dropcart\Api\Exceptions\Token\{
    EmptyKeyException,
    EnvKeysIdenticalException,
    KeyMissingException,
    KeysIdenticalException
};
use Lcobucci\Clock\SystemClock;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Encoding\ChainedFormatter;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Constraint\StrictValidAt;

class Token
{
    public const ENV_PUBLIC_KEY = 'DROPCART_PUBLIC_KEY';
    public const ENV_PRIVATE_KEY = 'DROPCART_PRIVATE_KEY';

    protected string $publicKey = '';
    protected string $privateKey = '';

    protected int $issuedAtOffset = 0;
    protected int $canOnlyBeUsedAfterOffset = 0;
    protected int $expirationOffset = 300;
    protected ?Configuration $configuration = null;

    /**
     * Set public and private key through environment variables. This is the recommended method.
     *
     * @throws EnvKeysIdenticalException
     * @throws KeyMissingException
     */
    public function setKeyPairFromEnvironment(
        string $envPublicKey = self::ENV_PUBLIC_KEY,
        string $envPrivateKey = self::ENV_PRIVATE_KEY
    ): self {
        if ($envPublicKey === $envPrivateKey) {
            throw new EnvKeysIdenticalException();
        }

        $publicKey = getenv($envPublicKey, true);
        $privateKey = getenv($envPrivateKey, true);

        if (empty($publicKey) || empty($privateKey)) {
            throw new KeyMissingException();
        }

        return $this
            ->setPublicKey($publicKey)
            ->setPrivateKey($privateKey);
    }

    public function setPublicKey(string $publicKey): self
    {
        $this->publicKey = $publicKey;
        return $this;
    }

    public function setPrivateKey(string $privateKey): self
    {
        $this->privateKey = $privateKey;
        return $this;
    }

    public function setIssuedAtOffset(int $issuedAtOffset): self
    {
        $this->issuedAtOffset = $issuedAtOffset;
        return $this;
    }

    public function setCanOnlyBeUsedAfterOffset(int $canOnlyBeUsedAfterOffset): self
    {
        $this->canOnlyBeUsedAfterOffset = $canOnlyBeUsedAfterOffset;
        return $this;
    }

    public function setExpirationOffset(int $expirationOffset): self
    {
        $this->expirationOffset = $expirationOffset;
        return $this;
    }

    public function get(): string
    {
        if (empty($this->publicKey) || empty($this->privateKey)) {
            throw new EmptyKeyException();
        }

        if ($this->publicKey === $this->privateKey) {
            throw new KeysIdenticalException();
        }

        $configuration = self::getConfiguration($this->privateKey);

        return $configuration->builder(ChainedFormatter::withUnixTimestampDates())
            ->issuedBy(
                $this->publicKey
            )
            ->issuedAt(
                CarbonImmutable::now()
                    ->addSeconds($this->issuedAtOffset)
            )
            ->canOnlyBeUsedAfter(
                CarbonImmutable::now()
                    ->addSeconds($this->canOnlyBeUsedAfterOffset)
            )
            ->expiresAt(
                CarbonImmutable::now()
                    ->addSeconds($this->expirationOffset)
            )
            ->getToken(
                $configuration->signer(),
                $configuration->signingKey()
            )
            ->toString();
    }

    public static function verify(string $token, string $privateKey): bool
    {
        $configuration = self::getConfiguration($privateKey);

        return $configuration->validator()
            ->validate(
                $configuration->parser()
                    ->parse($token),
                ...$configuration->validationConstraints()
            );
    }

    protected static function getConfiguration(string $privateKey): Configuration
    {
        $configuration = Configuration::forSymmetricSigner(
            self::getSigner(),
            self::getSigningKey($privateKey)
        );

        $configuration->setValidationConstraints(
            new SignedWith(self::getSigner(), self::getSigningKey($privateKey)),
            new StrictValidAt(SystemClock::fromUTC())
        );

        return $configuration;
    }

    protected static function getSigner(): Sha256
    {
        return new Sha256();
    }

    protected static function getSigningKey(string $privateKey): Key
    {
        return Key\InMemory::plainText($privateKey);
    }
}
