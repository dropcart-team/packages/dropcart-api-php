<?php

namespace Dropcart\Api\Traits;

use Dropcart\Api\Client;
use Dropcart\Api\Endpoints\EndpointAbstract;

trait GetEndpoint
{
    /** @var string */
    protected $resourcePath = '';

    /** @var EndpointAbstract[] */
    protected $endpoints = [];

    public function getEndpoint(string $className): EndpointAbstract
    {
        if (!isset($this->endpoints[$className])) {
            $this->endpoints[$className] = new $className($this->getClient(), $this->resourcePath);
        }

        return $this->endpoints[$className];
    }

    protected function getClient(): Client
    {
        /** @var $this Client */
        return $this;
    }
}