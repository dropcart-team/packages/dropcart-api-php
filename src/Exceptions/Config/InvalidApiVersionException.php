<?php

namespace Dropcart\Api\Exceptions\Config;

use Dropcart\Api\Exceptions\DropcartApiException;

class InvalidApiVersionException extends DropcartApiException
{
    protected $message = 'Invalid API version specification';
}