<?php

namespace Dropcart\Api\Exceptions\Config;

use Dropcart\Api\Exceptions\DropcartApiException;

class InsecureUriException extends DropcartApiException
{
    protected $message = 'API URI is insecure';
}