<?php

namespace Dropcart\Api\Exceptions\Config;

use Dropcart\Api\Exceptions\DropcartApiException;

class MalformedUriException extends DropcartApiException
{
    protected $message = 'API URI malformed, empty or unsupported protocol used or contained slash(es)';
}