<?php

namespace Dropcart\Api\Exceptions\Token;

use Dropcart\Api\Exceptions\DropcartApiException;

class KeysIdenticalException extends DropcartApiException
{
    protected $message = 'Public and private key should not be the same';
}