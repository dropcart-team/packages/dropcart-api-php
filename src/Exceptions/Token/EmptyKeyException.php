<?php

namespace Dropcart\Api\Exceptions\Token;

use Dropcart\Api\Exceptions\DropcartApiException;

class EmptyKeyException extends DropcartApiException
{
    protected $message = 'Public and/or private key not set';
}