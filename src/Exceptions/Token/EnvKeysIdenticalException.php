<?php

namespace Dropcart\Api\Exceptions\Token;

use Dropcart\Api\Exceptions\DropcartApiException;

class EnvKeysIdenticalException extends DropcartApiException
{
    protected $message = 'Environment variables should not be the same';
}