<?php

namespace Dropcart\Api\Exceptions\Token;

use Dropcart\Api\Exceptions\DropcartApiException;

class KeyMissingException extends DropcartApiException
{
    protected $message = 'Public and/or private key missing';
}