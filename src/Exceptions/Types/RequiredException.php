<?php

namespace Dropcart\Api\Exceptions\Types;

use Dropcart\Api\Exceptions\DropcartApiException;

class RequiredException extends DropcartApiException
{
    //
}