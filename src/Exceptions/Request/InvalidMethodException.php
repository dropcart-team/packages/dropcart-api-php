<?php

namespace Dropcart\Api\Exceptions\Request;

use Dropcart\Api\Exceptions\DropcartApiException;

class InvalidMethodException extends DropcartApiException
{
    protected $message = 'Invalid method';
}