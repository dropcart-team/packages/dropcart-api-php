<?php

namespace Dropcart\Api\Exceptions\Request;

use Dropcart\Api\Exceptions\DropcartApiException;

class InvalidResourceException extends DropcartApiException
{
    protected $message = 'Invalid resource';
}