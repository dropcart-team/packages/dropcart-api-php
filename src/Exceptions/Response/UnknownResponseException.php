<?php

namespace Dropcart\Api\Exceptions\Response;

use Dropcart\Api\Exceptions\DropcartApiException;

class UnknownResponseException extends DropcartApiException
{
    protected $message = 'Unknown response';
}