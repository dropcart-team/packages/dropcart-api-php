<?php

namespace Dropcart\Api\Exceptions\Response;

use Dropcart\Api\Exceptions\DropcartApiException;

class DuplicateModelException extends DropcartApiException
{

}