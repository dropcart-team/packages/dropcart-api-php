<?php

namespace Dropcart\Api\Exceptions\Response;

use Dropcart\Api\Exceptions\DropcartApiException;
use Throwable;

class PreconditionFailureException extends DropcartApiException
{
    /** @var array */
    protected $errors = [];

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $this->errors = $this->flatten(
            json_decode($message, true) ?: []
        );
        $message = implode(' ', $this->errors);

        parent::__construct($message, $code, $previous);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    protected function flatten(array $items): array
    {
        $flattened = [];

        array_walk_recursive($items, function ($item) use (&$flattened) {
            $flattened[] = $item;
        });

        return $flattened;
    }
}
