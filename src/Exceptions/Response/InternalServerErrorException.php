<?php

namespace Dropcart\Api\Exceptions\Response;

use Dropcart\Api\Exceptions\DropcartApiException;

class InternalServerErrorException extends DropcartApiException
{
    protected $message = 'Internal server error';
}