<?php

namespace Dropcart\Api\Exceptions\Resources;

use Dropcart\Api\Exceptions\DropcartApiException;

class InvalidDataException extends DropcartApiException
{
    protected $message = 'Supplied data is not an array with objects';
}