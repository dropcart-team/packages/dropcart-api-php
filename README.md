# Dropcart API connector for PHP

## Introduction

[Dropcart](https://www.dropcart.nl) is an e-commerce platform for drop shipping.
With this API connector, you can use your own solutions (like a web shop or supply
management system) to interact with the Dropcart platform.  

## Requirements

To use the Dropcart API connector, all you need are the public and private key.
These can be found in the settings of each supplier or shop you have created in
your account on the Dropcart website.

The Dropcart API connector requires PHP 7.2 at minimum. You may need to install
and enable the JSON module.

## Installation

The easiest way to install the Dropcart API connector is through [Composer](https://www.getcomposer.org).

```
$ composer require dropcart/dropcart-api-php
```

## Getting started

Initialize the Dropcart API connector:

```php
$token = (new \Dropcart\Api\Token())->setKeyPairFromEnvironment();
$dropcart = new \Dropcart\Api\Client($token);
```

The first line reads the public and private key (the *key pair*) from the
environment, which is the recommended way. By default the environment variables are
called `DROPCART_PUBLIC_KEY` and `DROPCART_PRIVATE_KEY`, but you can use others. See
the documentation for more details and another way to set the key pair.

## Example

Retrieve a paginated list of products:

```php
$products = $dropcart->supplier()->products()->list(new \Dropcart\Api\Types\Supplier\Product\Listing());
```

This will give you an `ProductCollection` instance. You can get the data through the
`getData()` method or simply iterate over the object with `foreach`:

```php
// returns array of ProductResource objects
$products->getData();

foreach ($products as $product) {
    // $product is a ProductResource
}
```

The `Listing` parameter instance contains methods to control what data you want to see
returned.
