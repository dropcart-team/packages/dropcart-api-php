<?php

require __DIR__ . '/../vendor/autoload.php';

try {
    $dotenv = \Dotenv\Dotenv::createImmutable(realpath(__DIR__ . '/..'));
    $dotenv->load();
} catch (\Dotenv\Exception\InvalidPathException $e) {
    die($e->getMessage() . PHP_EOL);
}

try {
    $token = (new \Dropcart\Api\Token())->setKeyPairFromEnvironment();
} catch (\Dropcart\Api\Exceptions\DropcartApiException $e) {
    die($e->getMessage() . PHP_EOL);
}

$client = new \Dropcart\Api\Client($token);