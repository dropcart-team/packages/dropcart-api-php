<?php

/**
 * Get one product.
 */

require __DIR__ . '/../initialize.php';

$supplierProduct = $client->supplier()->products()->get(4);

print "Supplier product ID: " . $supplierProduct->getId() . ' / ';
print "Product EAN: " . $supplierProduct->getProduct()->getEan() . ' / ';
print "Stock: " . $supplierProduct->getStock() . PHP_EOL;

foreach ($supplierProduct->getShippingCountries() as $shippingCountry) {
    print "Shipping country: " . $shippingCountry->getName() . ' / ';
    print "Shipping costs: " . $shippingCountry->getCosts() . PHP_EOL;
}
