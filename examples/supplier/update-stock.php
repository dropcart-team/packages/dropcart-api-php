<?php

/**
 * Update stock of supplier products.
 */

require __DIR__ . '/../initialize.php';

$supplierProductStock = new \Dropcart\Api\Types\Supplier\Product\Stock();
$supplierProductStock->add([
    'id' => 1,
    'stock' => 502,
]);
$supplierProductStock->add([
    'id' => 4,
    'stock' => 103,
]);

$client->supplier()->products()->updateStock($supplierProductStock);