<?php

/**
 * Update shipping details of a certain product in an order.
 */

require __DIR__ . '/../initialize.php';

$shipping = new \Dropcart\Api\Types\Supplier\Order\Shipping();
$shipping->setCourierId(1);
$shipping->setTrackingCode('FOOBAR123456');
$shipping->setDeliveredOn((new Carbon\Carbon())->addWeekdays(2));

$client->supplier()->orders()->updateProductShipping(102, 203, $shipping);

$supplierOrder = $client->supplier()->orders()->get(102);

foreach ($supplierOrder->getOrderProducts() as $orderProduct) {
    $details = $orderProduct->getShippingDetails();

    print "Courier name: " . $details->getCourierName() . PHP_EOL;
    print "Tracking code: "  . $details->getTrackingCode() . PHP_EOL;
    print "Delivered on: " . $details->getDeliveredOn() . PHP_EOL;
}

print PHP_EOL;