<?php

/**
 * Retrieve paginated list of supplier orders and show details about each order.
 */

require __DIR__ . '/../initialize.php';

$listing = new \Dropcart\Api\Types\Supplier\Order\Listing();
$listing->setPerPage(3);

$supplierOrders = $client->supplier()->orders()->list($listing);

print "Total number of orders: " . $supplierOrders->getTotalCount() . PHP_EOL;
print "Maximum number of orders per page: " . $supplierOrders->getPerPage() . PHP_EOL;
print "Current page: " . $supplierOrders->getCurrentPage() . PHP_EOL;
print "Number of orders  on this page: " . count($supplierOrders) . PHP_EOL;
print "Last page: " . $supplierOrders->getLastPage() . PHP_EOL;
print PHP_EOL;

foreach ($supplierOrders as $supplierOrder) {
    print "Supplier order ID: " . $supplierOrder->getId() . ' / ';
    print "Product count: " . $supplierOrder->getProductCount() . ' / ';
    print "Customer name: " . $supplierOrder->getCustomer()->getLastName() . PHP_EOL;

    foreach ($supplierOrder->getOrderProducts() as $orderProduct) {
        print "Order product ID: " . $orderProduct->getId() . ' / ';
        print "Product EAN: " . $orderProduct->getProduct()->getEan() . ' / ';
        print "Quantity: " . $orderProduct->getQuantity() . ' / ';
        print "Price including VAT: " . $orderProduct->getTotalPriceIn() . PHP_EOL;
    }

    print "Shipping costs: " . $supplierOrder->getShippingCosts()->getTotalPriceIn() . PHP_EOL;
    print "Total price including VAT: " . $supplierOrder->getTotalPriceIn() . PHP_EOL;
    print PHP_EOL;
}