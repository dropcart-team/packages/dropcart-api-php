<?php

/**
 * Retrieve paginated list of supplier products and show details about each product.
 */

require __DIR__ . '/../initialize.php';

$listing = new \Dropcart\Api\Types\Supplier\Product\Listing();
$listing->setPerPage(2);

$supplierProducts = $client->supplier()->products()->list($listing);

print "Total number of products: " . $supplierProducts->getTotalCount() . PHP_EOL;
print "Maximum number of products per page: " . $supplierProducts->getPerPage() . PHP_EOL;
print "Current page: " . $supplierProducts->getCurrentPage() . PHP_EOL;
print "Number of products on this page: " . count($supplierProducts) . PHP_EOL;
print "Last page: " . $supplierProducts->getLastPage() . PHP_EOL;
print PHP_EOL;

foreach ($supplierProducts as $supplierProduct) {
    print "Supplier product ID: " . $supplierProduct->getId() . ' / ';
    print "Product EAN: " . $supplierProduct->getProduct()->getEan() . ' / ';
    print "Stock: " . $supplierProduct->getStock() . PHP_EOL;

    foreach ($supplierProduct->getShippingCountries() as $shippingCountry) {
        print "Shipping country: " . $shippingCountry->getName() . ' / ';
        print "Shipping costs: " . $shippingCountry->getCosts() . PHP_EOL;
    }

    print PHP_EOL;
}

