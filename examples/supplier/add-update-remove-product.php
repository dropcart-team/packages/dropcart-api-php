<?php

/**
 * Add, update and remove supplier product.
 */

require __DIR__ . '/../initialize.php';

$supplierProduct = new \Dropcart\Api\Types\Supplier\Product\Product();

$supplierProduct->setEan('1234567890123');
$supplierProduct->setSku('9876543210');
$supplierProduct->setTranslations([
    [
        'locale' => 'en_GB',
        'name' => 'Test product',
    ]
]);
$supplierProduct->setCategories([
    [
        [
            'locale' => 'en_GB',
            'name' => 'Test category',
        ]
    ]
]);
$supplierProduct->setBrand('Testbrand');

$supplierProduct->setVat(['countryCode' => 'nl', 'percentage' => 21]);
$supplierProduct->setPriceExVat(25.55);
$supplierProduct->setMinPriceExVat(20.55);
$supplierProduct->setMaxPriceExVat(29.99);
$supplierProduct->setStock(255);

$addedProduct = $client->supplier()->products()->add($supplierProduct);

print "Added supplier product. ID: " . $addedProduct->getId() . PHP_EOL;
print "It has tax rule ID: " . $addedProduct->getTaxRuleId() . PHP_EOL;

$supplierProduct->setVat(['countryCode' => 'nl', 'percentage' => 6]);

$client->supplier()->products()->update($addedProduct->getId(), $supplierProduct);

print "Updated supplier product." . PHP_EOL;
print "It has tax rule ID: " . $client->supplier()->products()->get($addedProduct->getId())->getTaxRuleId() . PHP_EOL;

$client->supplier()->products()->remove($addedProduct->getId());

print "Removed supplier product." . PHP_EOL;
try {
    $client->supplier()->products()->get($addedProduct->getId());
} catch (\Dropcart\Api\Exceptions\Response\ModelNotFoundException $e) {
    print "Confirmed, supplier product could not be found." . PHP_EOL;
}
