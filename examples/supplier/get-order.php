<?php

/**
 * Get order and print some order data.
 */

require __DIR__ . '/../initialize.php';

$supplierOrder = $client->supplier()->orders()->get(101);

print "Supplier order ID: " . $supplierOrder->getId() . ' / ';
print "Product count: " . $supplierOrder->getProductCount() . ' / ';
print "Customer name: " . $supplierOrder->getCustomer()->getLastName() . PHP_EOL;

foreach ($supplierOrder->getOrderProducts() as $orderProduct) {
    print "Order product ID: " . $orderProduct->getId() . ' / ';
    print "Product EAN: " . $orderProduct->getProduct()->getEan() . ' / ';
    print "Quantity: " . $orderProduct->getQuantity() . ' / ';
    print "Price including VAT: " . $orderProduct->getTotalPriceIn() . PHP_EOL;
}

print "Shipping costs: " . $supplierOrder->getShippingCosts()->getTotalPriceIn() . PHP_EOL;
print "Total price including VAT: " . $supplierOrder->getTotalPriceIn() . PHP_EOL;
print PHP_EOL;
