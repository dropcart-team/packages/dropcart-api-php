<?php

/**
 * Retrieve exactly one product.
 */

require __DIR__ . '/../initialize.php';

$supplierProduct = $client->store()->products()->get(1);

print "Store product ID: " . $supplierProduct->getId() . PHP_EOL;
print "Product EAN: " . $supplierProduct->getProduct()->getEan() . PHP_EOL;
print "Minimum quantity: " . $supplierProduct->getMinimumQuantity() . PHP_EOL;
