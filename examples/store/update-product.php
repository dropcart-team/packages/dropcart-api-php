<?php

/**
 * Update store product.
 */

require __DIR__ . '/../initialize.php';

$product = new \Dropcart\Api\Types\Store\Product\Product();
$product->setMinimumQuantity(2);

$client->store()->products()->update(1, $product);