<?php

/**
 * Retrieve paginated list of store products and show details about each product.
 */

require __DIR__ . '/../initialize.php';

$listing = new \Dropcart\Api\Types\Store\Product\Listing();
$listing->setPerPage(2);

$storeProducts = $client->store()->products()->list($listing);

print "Total number of products: " . $storeProducts->getTotalCount() . PHP_EOL;
print "Maximum number of products per page: " . $storeProducts->getPerPage() . PHP_EOL;
print "Current page: " . $storeProducts->getCurrentPage() . PHP_EOL;
print "Number of products on this page: " . count($storeProducts) . PHP_EOL;
print "Last page: " . $storeProducts->getLastPage() . PHP_EOL;
print PHP_EOL;

foreach ($storeProducts as $storeProduct) {
    print "Store product ID: " . $storeProduct->getId() . ' / ';
    print "Product EAN: " . $storeProduct->getProduct()->getEan() . PHP_EOL;

    foreach ($storeProduct->getPriceDetails() as $priceDetail) {
        print "Supplier name: " . $priceDetail->getSupplierName() . ' / ';
        print "In stock: " . $priceDetail->getSupplierStock() . ' / ';
        print "Retail price excluding VAT: " . $priceDetail->getBasePriceEx() . PHP_EOL;

        foreach ($priceDetail->getShippingCosts() as $shippingCost) {
            print "Country ID: " . $shippingCost->getCountryId() . ' / ';
            print "Shipping price excluding VAT: " . $shippingCost->getBasePriceEx() . PHP_EOL;
        }

        $taxRule = $priceDetail->getTaxRule();
        print "Country ID: " . $taxRule->getCountryId() . ' / ';
        print "Tax rate: " . $taxRule->getRate() . PHP_EOL;
    }

    print PHP_EOL;
}